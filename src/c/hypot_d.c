#include "scicos_block4.h"
#include <math.h>

void hypot_d(scicos_block *block,int flag)
{
  double *u1;
  double *u2;
  double *y1;

  int nu,mu,i;

  mu=GetInPortRows(block,1);
  nu=GetInPortCols(block,1);
  u1=GetRealInPortPtrs(block,1);
  u2=GetRealInPortPtrs(block,2);
  y1=GetRealOutPortPtrs(block,1);
  for (i=0;i<mu*nu;i++) 
	{y1[i]=pow((pow(u1[i],2)+pow(u2[i],2)),0.5);}
}

