#include "scicos_block4.h"
#include <math.h>
#include <stdlib.h>
#include <machine.h>

// extern int C2F(wwpow1)();
 
void powblkz_m(scicos_block *block,int flag)
{
  if (flag==1) {
  double *ur,*ui;
  double *yr,*yi;
  double *ropar,*iopar;

  int nu,mu,i,nin,no,mo,m,n,inc1,inc2,mn,ierr;
  nin=GetNin(block);
  mu=GetOutPortRows(block,1);
  nu=GetOutPortCols(block,1);
  ur=GetRealInPortPtrs(block,1);
  ui=GetImagInPortPtrs(block,1);
  yr=GetRealOutPortPtrs(block,1);
  yi=GetImagOutPortPtrs(block,1);
  if (nin==1) 
     {ropar=GetRealOparPtrs(block,1);
      iopar=GetRealOparPtrs(block,2);
     mo=GetOparSize(block,1,1);
     no=GetOparSize(block,1,2);}
  else 
     {ropar=GetRealInPortPtrs(block,2);
     iopar=GetImagInPortPtrs(block,2);
     mo=GetInPortRows(block,2);
     no=GetInPortCols(block,2);}
  if ((mo*no)>1)
     {m=mo;
      n=no;
      inc2=1;
      if ((mu*nu)==1) inc1=0;
      else if ((mo==mu)&(no==nu)) inc1=1;
      else {
            sciprint("Inputs must have the same dimension");
            return;}}
  else{ 
      inc2=0;
      inc1=1;
      m=mu;
      n=nu;}
  mn=m*n;
  n=1;

abort();
//  C2F(wwpow1)(&mn,ur,ui,&inc1,ropar,iopar,&inc2,yr,yi,&n,&ierr);

  if (ierr != 0){
      set_block_error(-7);
      return;}  
 }
}

