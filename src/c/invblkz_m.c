#include "scicos_block4.h"
#include <machine.h>
#include <math.h>

void invblkz_m(scicos_block *block,int flag)
{
  double *ur,*ui;
  double *yr,*yi,den;
  int nu,mu,i;

  mu=GetInPortRows(block,1);
  nu=GetInPortCols(block,1);
  ur=GetRealInPortPtrs(block,1);
  ui=GetImagInPortPtrs(block,1);
  yr=GetRealOutPortPtrs(block,1);
  yi=GetImagOutPortPtrs(block,1);
  if ((flag==1)) {
  for(i=0;i<mu*nu;i++) 
     {den=pow(ui[i],2)+pow(ur[i],2);
     if (den==0){
	 set_block_error(-7);
         return;}
     yr[i]=ur[i]/den;
     yi[i]=-ui[i]/den;}
  }
}
