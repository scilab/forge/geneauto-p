#include "scicos_block4.h"
#include <math.h>

#define rem(u1,u2,y1,mu,nu) { for (i=0;i<mu*nu;i++)\
	                       {if (u2[i]!=0)\
                                 {if (((u1[i]>0) & (u2[i]>0))|((u1[i]<0) & (u2[i]<0)))\
                                     *(y1+i)=u1[i]-((((floor)(u1[i]/u2[i]))*u2[i]));\
				  else\
                                     *(y1+i)=u1[i]-((((ceil)(u1[i]/u2[i]))*u2[i]));}\
                                else\
                                     {set_block_error(-2);\
				     return;}}}

void remains(scicos_block *block,int flag)
{  if (flag==1){
  int nu,mu,i,ut;
  mu=GetOutPortRows(block,1);
  nu=GetOutPortCols(block,1);
  ut=GetInType(block,1);
  switch (ut)
  {
      case SCSREAL_N :{
           double *u1,*u2,*y1;
           u1=GetRealInPortPtrs(block,1);
           u2=GetRealInPortPtrs(block,2);
           y1=GetRealOutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}

      case SCSINT32_N :{
           long *u1,*u2,*y1;
           u1=Getint32InPortPtrs(block,1);
           u2=Getint32InPortPtrs(block,2);
           y1=Getint32OutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}

      case SCSINT16_N :{
           short *u1,*u2,*y1;
           u1=Getint16InPortPtrs(block,1);
           u2=Getint16InPortPtrs(block,2);
           y1=Getint16OutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}

      case SCSINT8_N :{
           char *u1,*u2,*y1;
           u1=Getint8InPortPtrs(block,1);
           u2=Getint8InPortPtrs(block,2);
           y1=Getint8OutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}

      case SCSUINT32_N :{
           unsigned long *u1,*u2,*y1;
           u1=Getuint32InPortPtrs(block,1);
           u2=Getuint32InPortPtrs(block,2);
           y1=Getuint32OutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}

      case SCSUINT16_N :{
           unsigned short *u1,*u2,*y1;
           u1=Getuint16InPortPtrs(block,1);
           u2=Getuint16InPortPtrs(block,2);
           y1=Getuint16OutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}

      case SCSUINT8_N :{
           unsigned char *u1,*u2,*y1;
           u1=Getuint8InPortPtrs(block,1);
           u2=Getuint8InPortPtrs(block,2);
           y1=Getuint8OutPortPtrs(block,1);
           rem(u1,u2,y1,mu,nu);
           break;}
        
      default:{ 
	  set_block_error(-4);
          return;}
  }
 }
}

