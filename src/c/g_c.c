#include "scicos_block4.h" 

void g_c(scicos_block *block,int flag)
{ if (flag==1){
    int i_x,i_y,mo,no;
    double *u1,*u2,*y1,*opar,a,b;
    u1=GetRealInPortPtrs(block,1);
    u2=GetRealInPortPtrs(block,2);
    y1=GetRealOutPortPtrs(block,1);
    opar=GetRealOparPtrs(block,1);
    mo=GetOparSize(block,1,1);
    no=GetOparSize(block,1,2);
    if (*u1 < *(opar+mo)) i_x=1;
    else if (*u1>*(opar+mo*(no-1))) i_x=no-1;
    else{
	for (i_x=0;i_x<no-2;i_x++){
           if ((*u1 >= *(opar+i_x*mo)) & (*u1 < *(opar+(i_x*mo)+1))){
	       break;}}}
    if (*u2 < *(opar+1)) i_y=1;
    else if (*u2>*(opar+mo-1)) i_y=no-1;
    else{
	for (i_y=0;i_y<mo-2;i_y++){
           if ((*u2 >= *(opar+i_y*mo)) & (*u2 < *(opar+(i_y*mo)+1))){
	       break;}}}
    *y1=*(opar+i_x+(i_y*mo));
    if ((*u2<=(*(opar+1)))|(*u2>=(*(opar+mo-1)))){
	if ((*u1>(*(opar+mo)))&(*u1<(*(opar+(no-1)*mo)))){
	    *y1=*y1+((*u1-(*(opar+i_x*mo)))*(((*(opar+1+i_x+(i_y*mo)))-(*(opar+i_x+(i_y*mo))))/((*(opar+(1+i_x)*mo))-(*(opar+i_x*mo)))));}}
    else{
        if ((*u1<=(*(opar+mo)))|(*u1>=(*(opar+(no-1)*mo)))){
	    *y1=*y1+((*u2-(*(opar+i_y)))*(((*(opar+i_x+((1+i_y)*mo)))-(*(opar+i_x+(i_y*mo))))/((*(opar+(1+i_y)))-(*(opar+i_y)))));}
	else{
            a=*y1+((*u1-(*(opar+i_x*mo)))*(((*(opar+1+i_x+(i_y*mo)))-(*(opar+i_x+(i_y*mo))))/((*(opar+(1+i_x)*mo))-(*(opar+i_x*mo)))));
            b=(*(opar+i_x+((1+i_y)*mo)))+((*y1-(*(opar+i_x*mo)))*(((*(opar+1+i_x+((1+i_y)*mo)))-(*(opar+i_x+((1+i_y)*mo))))/((*(opar+(1+i_x)*mo))-(*(opar+i_x*mo)))));
            *y1=a+((*u2-(*(opar+i_y)))*((b-a)/((*(opar+1+i_y))-(*(opar+i_y)))));}}
 }
}
