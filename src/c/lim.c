#include "scicos_block4.h"

void lim(scicos_block *block,int flag)
{
    int nin;
    double *rpar;
    double *y1,*u1,max_val,min_val;
    u1=GetRealInPortPtrs(block,1);
    y1=GetRealOutPortPtrs(block,1);
    nin=GetNin(block);
    rpar=GetRparPtrs(block);
    if (flag==1){
    if (nin==1) 
    {max_val=*(rpar+1);
    min_val=*rpar;}
    else
    {double *u2,*u3;
    u2=GetRealInPortPtrs(block,2);
    u3=GetRealInPortPtrs(block,3);
    max_val=*u3;
    min_val=*u2;}  
    if ((max_val)>=(min_val))
        if (*u1 > max_val){
	    *y1 = max_val;
	} else if (*u1 < min_val){
		*y1 = min_val;
        } else {
		*y1 = *u1;
	} 
     else {
	 *y1 = max_val;}
    }
}
