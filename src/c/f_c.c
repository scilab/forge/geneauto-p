#include "scicos_block4.h" 

void f_c(scicos_block *block,int flag)
{ if (flag==1){
    int i,mo,no;
    double *u1,*y1,*opar;
    u1=GetRealInPortPtrs(block,1);
    y1=GetRealOutPortPtrs(block,1);
    opar=GetRealOparPtrs(block,1);
    mo=GetOparSize(block,1,1);
    no=GetOparSize(block,1,2);
    if (*u1 < *opar) *y1=*(opar+mo);
    else if (*u1>*(opar+mo-1)) *y1=*(opar+mo*no-1);
    else{
	for (i=0;i<mo-2;i++){
           if ((*u1 >= *(opar+i)) & (*u1 < *(opar+i+1))){
	       break;}}
        *y1=*(opar+i+mo)+((*u1-*(opar+i))*((*(opar+i+1+mo)-(*(opar+i+mo)))/(*(opar+i+1)-(*(opar+i)))));}
 }
}
