#include <memory.h>
#include "scicos_block4.h"
#include<stdio.h>

void samp_hold(scicos_block *block,int flag)
{ 
  /* Copyright INRIA */
  void *u1,*u4,*u5,*y1,*y2;
  int mu,nu,su,cond3,cond2;
  mu=GetInPortRows(block,1);
  nu=GetInPortCols(block,1);
  su=GetSizeOfIn(block,1);
  u1=GetInPortPtrs(block,1);
  u4=GetInPortPtrs(block,4);
  u5=GetInPortPtrs(block,5);
  y1=GetOutPortPtrs(block,1);
  y2=GetOutPortPtrs(block,2);
  switch (GetInType(block,3)){
	case SCSREAL_N   :{
			  double *u3;
			  u3=GetRealInPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	case SCSCOMPLEX_N :{
			  double *u3r,*u3i;
			  u3r=GetRealInPortPtrs(block,3);
			  u3i=GetImagInPortPtrs(block,3);
			  cond3=((*u3r!=0) & (*u3i!=0));
			  break;}
	case SCSINT32_N   :{
			  long *u3;
			  u3=Getint32InPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	case SCSINT16_N   :{
			  short *u3;
			  u3=Getint16InPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	case SCSINT8_N   :{
			  char *u3;
			  u3=Getint8InPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	case SCSUINT32_N   :{
			  unsigned long *u3;
			  u3=Getuint32InPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	case SCSUINT16_N   :{
			  unsigned short *u3;
			  u3=Getuint16InPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	case SCSUINT8_N   :{
			  unsigned char *u3;
			  u3=Getuint8InPortPtrs(block,3);
			  cond3=(*u3!=0);
			  break;}
	}
  switch (GetInType(block,2)){
	case SCSREAL_N   :{
			  double *u2;
			  u2=GetRealInPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	case SCSCOMPLEX_N   :{
			  double *u2r,*u2i;
			  u2r=GetRealInPortPtrs(block,2);
			  u2i=GetImagInPortPtrs(block,2);
			  cond2=((*u2r!=0) & (*u2i!=0));
			  break;}
	case SCSINT32_N   :{
			  long *u2;
			  u2=Getint32InPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	case SCSINT16_N   :{
			  short *u2;
			  u2=Getint16InPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	case SCSINT8_N   :{
			  char *u2;
			  u2=Getint8InPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	case SCSUINT32_N   :{
			  unsigned long *u2;
			  u2=Getuint32InPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	case SCSUINT16_N   :{
			  unsigned short *u2;
			  u2=Getuint16InPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	case SCSUINT8_N   :{
			  unsigned char *u2;
			  u2=Getuint8InPortPtrs(block,2);
			  cond2=(*u2!=0);
			  break;}
	}
  if (cond3)
     {memcpy(y1,u4,mu*nu*su);
      memcpy(y2,u4,mu*nu*su);}
  else if (cond2) 
     {memcpy(y1,u1,mu*nu*su);
      memcpy(y2,u1,mu*nu*su);}
  else
     {memcpy(y1,u5,mu*nu*su);
      memcpy(y2,u5,mu*nu*su);}
}
