#include "scicos_block4.h"

void bascr(scicos_block *block,int flag)
{
    int nin;
    int *ipar;
    long *oz;
    long *y1,*u1,*u2;
    u1=Getint32InPortPtrs(block,1); 
    u2=Getint32InPortPtrs(block,2);
    y1=Getint32OutPortPtrs(block,1);
    oz=Getint32OzPtrs(block,1);
    nin=GetNin(block);
    ipar=GetIparPtrs(block);
    if (flag==1){
    if (nin==2)  
     {if (*(ipar+1)!=0)
        *y1=(long) *ipar;
      else if ((*u2>0))
	 *y1=0;
      else if ((*u1>0))
         *y1=1;
      else 
          *y1=*oz;}

    else 
     {long *u3,*u4;
     u3=Getint32InPortPtrs(block,3);
     u4=Getint32InPortPtrs(block,4); 
     if (*(u4)!=0)
        *y1=(long) *u3;
     else if ((*u2>0))
	 *y1=0;
     else if (*u1>0)
         *y1=1;
     else 
         *y1=*oz;}
/*    }
      else if (flag==2){*/
	*oz=*y1;}
}
