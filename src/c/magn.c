#include "scicos_block4.h"
#include <math.h>
void magn(scicos_block *block,int flag)
{
  double *u1r;
  double *u1i;
  double *y1;

  int nu,mu,i;

  mu=GetOutPortRows(block,1);
  nu=GetOutPortCols(block,1);
  u1r=GetRealInPortPtrs(block,1);
  u1i=GetImagInPortPtrs(block,1);
  y1=GetRealOutPortPtrs(block,1);
  for (i=0;i<mu*nu;i++) 
	{*(y1+i)=(pow(*(u1r+i),2)+pow(*(u1i+i),2));}
}

