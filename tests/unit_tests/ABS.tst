function perform()
    name = "ABS";
    [macros,path] = libraryinfo("ga_blockslib")
    path = path + filesep() + ".." + filesep() + ".." + filesep() ..
           + "tests" + filesep() + "unit_tests" + filesep() + name + filesep();
    
    // import the diagram
    assert_checktrue(importXcosDiagram(path + name + ".xcos"));
    
    // locate the diagram to test
    blk = [];
    for i=1:length(scs_m.objs)
        blk = scs_m.objs(i);
        if typeof(blk) == "Block" & blk.gui == "SUPER_f" then
            if blk.model.rpar.props.title == name then
                break;
            end
        end
    end
    assert_checktrue(typeof(blk) == "Block");
    assert_checktrue(blk.gui == "SUPER_f");
    assert_checktrue(blk.model.rpar.props.title == name);
    
    // check xml file
    FindSBParams=ga_FindSBParams;
    main_scs_m=ga_replaceports(blk.model.rpar);
    [ok,xml,capt,actt,freof]=ga_scicos2xml(main_scs_m);
    
    assert_checktrue(ok)
    
    // compare the xml files
    ref=mgetl(path + name + ".gsm.xml.ref");
    
    // filter out the date and position data
    dateLines = [5 10 74];
    xml(dateLines) = ref(dateLines);
    
    // check
    ii = find(~(ref == xml));
    assert_checktrue(isempty(ii));
endfunction

perform();
clear perform

