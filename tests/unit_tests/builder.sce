mode(-1)
p=get_absolute_file_path('builder.sce')

tests=['ABS','BASC','conv','div','ifthel2','ifthel4','m5','modul','mux','pulse','scgen','t2','t5','t5_1','ud']

for t=tests
  mprintf('Building dynamic library for test :%s\n',t)
  exec(p+t+'/src/builder.sce',-1)
end
