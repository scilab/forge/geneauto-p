/*
    ABS.c
    Generated by Gene-Auto toolset ver 2.4.9
    (launcher GALauncherSCICOS)
    Generated on: 25/04/2012 17:52:29.742
    source model: ABS
    model version: 6.3
    last saved by:
    last saved on:
*/

/* Includes */

#include "ABS.h"

/* Function definitions */

void ABS_init() {
}

void ABS_compute(t_ABS_io *_io_) {
    /*  Output from <SystemBlock: name=ABS>/<SourceBlock: name=B1>/<OutDataPort: name=>  */
    REAL B1;
    /*  Output from <SystemBlock: name=ABS>/<CombinatorialBlock: name=B2>/<OutDataPort: name=>  */
    REAL B2;
    /*  START Block: null  */
    B1 = -1.0;
    /*  END Block: null  */
    /*  START Block: null  */
    B2 = fabs(B1);
    /*  END Block: null  */
    /*  START Block: null  */
    _io_->B3 = B2;
    /*  END Block: null  */
}

