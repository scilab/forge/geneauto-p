
#ifndef __ABS__
#define __ABS__

/* Includes */

#include "GATypes.h"
#include "ABS_types.h"
#include <math.h>


/* Function prototypes */

extern void ABS_init();
extern void ABS_compute(t_ABS_io *_io_);


#endif
