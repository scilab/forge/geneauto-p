/*
    Counter.c
    Generated by Gene-Auto toolset ver 2.4.9
    (launcher GALauncherSCICOS)
    Generated on: 15/06/2012 14:17:49.239
    source model: T5
    model version: 6.3
    last saved by:
    last saved on:
*/

/* Includes */

#include "Counter.h"

/* Function definitions */

void Counter_init(t_T5_state *_state_) {
    /*  START Block: -26d26ed8:137ef86bff0:-7e9c  */
    _state_->Counter_3_memory = 0;
    /*  END Block: -26d26ed8:137ef86bff0:-7e9c  */
}

void Counter_compute(t_Counter_io *_io_, t_T5_state *_state_) {
    /*  Output from -26d26ed8:137ef86bff0:-7ea2/<OutDataPort: name=>  */
    INT32 Counter_1;
    /*  Output from -26d26ed8:137ef86bff0:-7ea0/<OutDataPort: name=>  */
    INT32 Counter_2;
    /*  Output from -26d26ed8:137ef86bff0:-7e9c/<OutDataPort: name=>  */
    INT32 Counter_3;
    /*  Output from -26d26ed8:137ef86bff0:-7e98/<OutDataPort: name=>  */
    REAL Counter_4;
    /*  START Block: -26d26ed8:137ef86bff0:-7ea2  */
    Counter_1 = 1;
    /*  END Block: -26d26ed8:137ef86bff0:-7ea2  */
    /*  START Block: -26d26ed8:137ef86bff0:-7e9c  */
    Counter_3 = _state_->Counter_3_memory;
    /*  END Block: -26d26ed8:137ef86bff0:-7e9c  */
    /*  START Block: -26d26ed8:137ef86bff0:-7ea0  */
    Counter_2 = Counter_3 + Counter_1;
    /*  END Block: -26d26ed8:137ef86bff0:-7ea0  */
    /*  START Block: -26d26ed8:137ef86bff0:-7e98  */
    Counter_4 = (INT32) Counter_2;
    /*  END Block: -26d26ed8:137ef86bff0:-7e98  */
    /*  START Block: null  */
    _io_->Counter_6 = Counter_4;
    /*  END Block: null  */
    /*  START Block memory write: -26d26ed8:137ef86bff0:-7e9c  */
    _state_->Counter_3_memory = Counter_2;
    /*  END Block memory write: -26d26ed8:137ef86bff0:-7e9c  */
}

