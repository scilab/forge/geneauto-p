
#ifndef __T5__
#define __T5__

/* Includes */

#include "GATypes.h"
#include "T5_types.h"
#include "Counter.h"
#include <math.h>


/* Variable Declarations */

extern REAL T5_Counter_Counter_6;
extern t_Counter_io _Counter_io;

/* Function prototypes */

extern void T5_init(t_T5_state *_state_);
extern void T5_compute(t_T5_io *_io_, t_T5_state *_state_);


#endif
