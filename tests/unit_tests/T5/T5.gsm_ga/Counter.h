
#ifndef __Counter__
#define __Counter__

/* Includes */

#include "GATypes.h"
#include "T5_types.h"


/* Function prototypes */

extern void Counter_init(t_T5_state *_state_);
extern void Counter_compute(t_Counter_io *_io_, t_T5_state *_state_);


#endif
