// Copyright (C) 2011 - DIGITEO

// This file is released under the 3-clause BSD license. See COPYING-BSD.

function [ga_blockslib, ga_cosf_generatorlib, ga_cosf_block_cosflib, ..
          ga_xml_generatorlib, ga_xml_block_tranlib, ..
          geneauto_xcoslib] = startModule()

  mprintf("Start Xcos Geneauto toolset\n");

  if isdef("geneauto_xcoslib") then
    warning("Xcos Geneauto toolset library is already loaded");
    return;
  end

// check minimal version (xcosPal required)
// =============================================================================
  if ~isdef('xcosPal') then
    // and xcos features required
    error(gettext('Scilab 5.3.2 or more is required.'));
  end
// =============================================================================  
// force to load some libraries (dependancies)
  loadXcosLibs(); loadScicos();
// =============================================================================
  etc_tlbx  = get_absolute_file_path("geneauto_xcos.start");
  etc_tlbx  = getshortpathname(etc_tlbx);
  root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\etc\") );

// Load functions library
// =============================================================================
  mprintf("\tLoad macros\n");
  pathmacros = pathconvert( root_tlbx ) + "macros" + filesep();

  subdirs = ["GeneAutoPal" "cosf_generator" ("cosf_generator" + filesep() + "block_cosf") ..
             "xml_generator" ("xml_generator" + filesep() + "block_tran")];
  sublibs = ["ga_blockslib" "ga_cosf_generatorlib" "ga_cosf_block_cosflib" ..
             "ga_xml_generatorlib" "ga_xml_block_tranlib"];

  geneauto_xcoslib = list();
  for i=1:size(subdirs, '*')
    libpath = pathmacros + subdirs(i) + filesep();
    execstr(sublibs(i) + " = lib(libpath);");

    geneauto_xcoslib($+1) = evstr(sublibs(i));
  end

// Add blocks to the Xcos palette
// =============================================================================
  mprintf("\tLoad palette\n");
  pal = xcosPal("Xcos Geneauto blocks");

  h5Files = gsort(ls(root_tlbx + "/images/h5/*.sod"));
  gifFiles = gsort(ls(root_tlbx + "/images/gif/*.gif"));
  svgFiles = gsort(ls(root_tlbx + "/images/svg/*.svg"));

  for i=1:size(h5Files, "*")
    pal = xcosPalAddBlock(pal, h5Files(i), gifFiles(i), svgFiles(i));
  end

  if ~xcosPalAdd(pal) then
    error(msprintf(gettext("%s: Unable to export %s.\n"), "geneauto_xcos.start", "pal"));
  end

// Load simulation functions
// =============================================================================
  mprintf("\tLoad simulations functions\n");
  verboseMode = ilib_verbose();
  ilib_verbose(0);
  exec(pathconvert(root_tlbx+"/src/c/loader.sce", %f));
  ilib_verbose(verboseMode);

// Load and add help chapter
// =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    mprintf("\tLoad help\n");
    path_addchapter = pathconvert(root_tlbx+"/jar");
    if ( isdir(path_addchapter) <> [] ) then
        add_help_chapter("Xcos Geneauto toolset", path_addchapter, %F);
    end
  end

// Load demos
// =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    mprintf("\tLoad demos\n");
    pathdemos = pathconvert(root_tlbx+"/demos/geneauto_xcos.dem.gateway.sce", %F, %T);
    add_demo("Xcos Geneauto toolset", pathdemos);
  end

// Add menu to the tools menu
// ============================================================================
  try
    [macros,path]=libraryinfo("ga_xml_generatorlib");

    // Geneauto XML generation
    xcosAddToolsMenu(gettext("Xcos to GeneAuto"), "toGeneAuto_()");    [macros,path]=libraryinfo("ga_xml_generatorlib");

    // Geneauto external optional dependency
    geneauto_root=fullpath(sprintf("%s../../external/geneauto/", path));
    geneauto_version="2.4.9"

    launcherjar=geneauto_root+"geneauto.galauncher-"+geneauto_version+".jar";
    if isfile(launcherjar) then
        xcosAddToolsMenu(gettext("Xcos to C (GeneAuto)"), "toGeneAutoC_()");
    else
        warning("GeneAuto "+geneauto_version+" not found.");
    end
  catch
    warning("xcosAddToolsMenu not available")
  end

endfunction

if with_module('xcos') then
  [ga_blockslib, ga_cosf_generatorlib, ga_cosf_block_cosflib, ..
   ga_xml_generatorlib, ga_xml_block_tranlib, ..
   geneauto_xcoslib] = startModule();
  clear startModule; // remove startModule on stack
end
