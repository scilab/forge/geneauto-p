
#ifndef __Subsystem__
#define __Subsystem__

/* Includes */

#include "GATypes.h"
#include "Subsystem_types.h"
#include "B3_7.h"
#include "B3_6.h"
#include "B3.h"


/* Variable Declarations */

extern REAL Subsystem_B3_B3_12[2];
extern t_B3_io _B3_io;

/* Function prototypes */

extern void Subsystem_init(t_Subsystem_state *_state_);
extern void Subsystem_compute(t_Subsystem_io *_io_, t_Subsystem_state *_state_);


#endif
