
#ifndef __B3_7__
#define __B3_7__

/* Includes */

#include "GATypes.h"
#include "Subsystem_types.h"


/* Function prototypes */

extern void B3_7_init(t_Subsystem_state *_state_);
extern void B3_7_compute(t_B3_7_io *_io_, t_Subsystem_state *_state_);


#endif
