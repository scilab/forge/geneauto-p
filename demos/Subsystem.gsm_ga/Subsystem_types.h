
#ifndef __Subsystem_types__
#define __Subsystem_types__

/* Includes */

#include "GATypes.h"


/* Type declarations */

typedef struct {
    REAL B3_1;
    REAL B3_12[2];
} t_B3_io;

typedef struct {
    REAL B3_7_1;
    REAL B3_7_4;
} t_B3_7_io;

typedef struct {
    REAL B3_6_1;
    REAL B3_6_3;
} t_B3_6_io;

typedef struct {
    REAL B1;
    REAL B4[2];
} t_Subsystem_io;

typedef struct {
    /*  Block: -35079f4:137f00fab4d:-7c9a  */
    REAL B3_7_3_memory;
    /*  Block: -35079f4:137f00fab4d:-7cae  */
    REAL B3_6_2_memory;
    /*  Block: -35079f4:137f00fab4d:-7ce2  */
    REAL B3_11_memory;
} t_Subsystem_state;



#endif
