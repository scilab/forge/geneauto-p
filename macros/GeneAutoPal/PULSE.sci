function [x,y,typ]=PULSE(job,arg1,arg2)
//
// Copyright INRIA
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  [x,y,typ]=standard_outputs(arg1)
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1
  model=x.model;
  graphics=x.graphics
  exprs=graphics.exprs
  num1=model.rpar
  [ln,fun]=where();
  if find (fun=="SUPER_f")<>[] then
    v=find (fun=="SUPER_f")
    fun=fun(1:v(1)-1)
  end
  if or(fun=="clickin") then
     num=x_choose(['internal';'external'],['Choose whether you want the Init and B_init';..
					  'to be parameters block or external input']);
  else
     num=num1
  end
  if num==0 then return;end   
  while %t then
  if num<>1 then
    model.rpar=num     
     [ok,rule,exprs($)]=getvalue('Set Pulse parameter',..
	    ['Rule (1=rising 2=falling)'],list('vec',1),exprs($))
    if ~ok then break,end
    in=[1 1;1 1;1 1];it=[9 9 9]
    model.ipar=rule
  else
    model.rpar=num
    [ok,init,b_init,rule,exprs]=getvalue('Set Pulse parameter',..
	    ['Init (1=true,0=false)';'B_init (1=true,0=false)';'Rule (1=rising 2=falling)'],list('vec',1,'vec',1,'vec',1),exprs)
    if ~ok then break,end    
    if and(init<>[0 1]) then message('Init must be only 0 or 1'),ok=%f;end
    if and(b_init<>[0 1]) then message('B_init must be only 0 or 1'),ok=%f;end
    in=[1 1];it=9
    if ok then
       model.ipar=[init;b_init;rule]
    end
  end
  if (rule<>1 & rule<>2) then message('Rule must be 0 or 1');ok=%f;end
  if ok then
    x0=0;
    out=[1 1];ot=9
    model.odstate=list(int8(x0))
    reinit=0;
    [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),ones(reinit,1),[])
    graphics.exprs=exprs
    x.model=model;x.graphics=graphics;
    arg1=x;
    break;
   end
  end
case 'define' then
  model=scicos_model()
   init=0;b_init=0;rule=1;x0=0;
  junction_name='pulse';
  funtyp=4;
  model.sim=list(junction_name,funtyp)
  model.in=[1;1;1]
  model.in2=[1;1;1]
  model.intyp=[9 9 9]
  model.out=1
  model.out2=1
  model.outtyp=9
  model.evtin=[]
  model.evtout=[]
  model.state=[]
  model.odstate=list(int8(x0))
  model.dstate=[]
  model.rpar=2
  model.ipar=[init;b_init;rule]
  model.blocktype='c' 
  model.firing=[]
  model.dep_ut=[%t %f]
  exprs=[sci2exp(init);sci2exp(b_init);sci2exp(rule)]
  gr_i=['[x,y,typ]=standard_inputs(o) ';
	'dd=sz(1)/10,de=9*sz(1)/10';
	'if size(y,''*'')==3 then ';
	'xstring(orig(1)+dd/2,y(1)-4,''E'')';
	'xstring(orig(1)+dd/2,y(2)-4,''Init'')';
	'xstring(orig(1)+dd/2,y(3)-4,''BInit'')';
	'else ';
	'xstring(orig(1)+dd/2,y(1)-4,''E'')';
	'end ';
	'[x,y,typ]=standard_outputs(o) ';
	'xstring(orig(1)+sz(1)-dd,y(1)-4,''S'')';
	'xstringb(orig(1)+sz(1)/3,orig(2),''PULSE'',2*sz(1)/3-sz(1)/10,sz(2)*9/10,''fill'')';
        'if arg1.model.ipar($)==1 then ';
        'x=orig(1)*ones(4,1)+sz(1)/3*[0;1/2;1/2;1]+sz(1)/10';
        'y=orig(2)*ones(4,1)+(4/5)*sz(2)*[0;0;1;1]+sz(2)/10';
	'else ';
	'x=orig(1)*ones(4,1)+sz(1)/3*[0;1/2;1/2;1]+sz(1)/10';
	'y=orig(2)*ones(4,1)+(4/5)*sz(2)*[1;1;0;0]+sz(2)/10';
	'end';
	'xpoly(x,y)']
  x=standard_define([5 3],model,exprs,gr_i)
end
endfunction

