function [x,y,typ]=DelayRE(job,arg1,arg2)
//
// Copyright INRIA
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  [x,y,typ]=standard_outputs(arg1)
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1
case 'define' then
  model=scicos_model()
  junction_name='delayre';
  funtyp=4;
  model.sim=list(junction_name,funtyp)
  model.in=[-1;1;1;-1;-1]
  model.in2=[-2;1;1;-2;-2]
  model.intyp=[-1 -2 -3 -1 -1]
  model.out=[-1;-1]
  model.out2=[-2;-2]
  model.outtyp=[-1 -1]
  model.evtin=[]
  model.evtout=[]
  model.state=[]
  model.dstate=[]
  model.rpar=[]
  model.ipar=[]
  model.blocktype='c' 
  model.firing=[]
  model.dep_ut=[%t %f]
  label=[sci2exp(1)];
  gr_i=['[x,y,typ]=standard_inputs(o) ';
	'dd=sz(1)/10,de=9*sz(1)/10';
	'xstring(orig(1)+dd,y(1)-4,''u'')';
	'xstring(orig(1)+dd,y(2)-4,''E'')';
	'xstring(orig(1)+dd,y(3)-4,''R'')';
	'xstring(orig(1)+dd,y(4)-4,''IV'')';
	'xstring(orig(1)+dd,y(5)-4,''x_In'')';
	'[x,y,typ]=standard_outputs(o) ';
	'xstring(orig(1)+sz(1)-dd,y(1)-4,''y'')';
	'xstring(orig(1)+sz(1)-3*dd,y(2)-4,''x_Out'')'
	'xstringb(orig(1)+2*dd,orig(2),''DelayRE'',sz(1)-4*dd,sz(2),''fill'');']
  x=standard_define([5 4],model,label,gr_i)
end
endfunction

