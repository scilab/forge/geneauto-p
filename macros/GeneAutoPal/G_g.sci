function [x,y,typ]=G_g(job,arg1,arg2)
//
// Copyright INRIA
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  [x,y,typ]=standard_outputs(arg1)
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1
  model=x.model;
  graphics=x.graphics
  exprs=graphics.exprs
  tab=model.opar(1)
  while %t then
    [ok,tab,exprs]=getvalue(['Set G parameter';'the Table must be a 11x11 Matrix';..
	                     'It is recommended to define the matrix in the context'],..
	    ['Table'],list('mat',[11,11]),exprs)
    if ~ok then break,end
    model.opar=list(tab)
    if ok then
     graphics.exprs=exprs
     x.model=model;x.graphics=graphics;
     arg1=x;
     break;
   end
  end
case 'define' then
  model=scicos_model()
  A=rand(11,11);
  [v,k]=gsort(A(:,1),'g','i');
  tab=A(k,:);
  junction_name='g_c';
  funtyp=4;
  model.sim=list(junction_name,funtyp)
  model.in=[1;1]
  model.in2=[1;1]
  model.intyp=[1 1]
  model.out=1
  model.out2=1
  model.outtyp=1
  model.evtin=[]
  model.evtout=[]
  model.state=[]
  model.dstate=[]
  model.ipar=[]
  model.opar=list(tab)
  model.blocktype='c' 
  model.firing=[]
  model.dep_ut=[%t %f]
  exprs=['tab']
  gr_i=['xstringb(orig(1),orig(2),''G'',sz(1),sz(2),''fill'')';]
  x=standard_define([2 2],model,exprs,gr_i)
end
endfunction

