function [x,y,typ]=BascR(job,arg1,arg2)
//
// Copyright INRIA
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  [x,y,typ]=standard_outputs(arg1)
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1
  model=x.model;
  graphics=x.graphics
  exprs=graphics.exprs
  num1=model.rpar
  [ln,fun]=where();
  if find (fun=="SUPER_f")<>[] then
    v=find (fun=="SUPER_f")
    fun=fun(1:v(1)-1)
  end
  if or(fun=="clickin") then
     num=x_choose(['internal';'external'],['Choose whether you want the Init and B_init';..
					  'to be parameters block or external input']);
  else
     num=num1
  end
  if num==0 then return;end   
  while %t then				  
  if num<>1 then
    model.rpar=num
    in=[1 1;1 1;1 1;1 1];it=[9 9 9 9]
    graphics.gr_i=['[x,y,typ]=standard_inputs(o) ';
	'dd=sz(1)/10,de=9*sz(1)/10';
	'xstring(orig(1)+dd,y(1)-4,''S'')';
	'xstring(orig(1)+dd,y(2)-4,''R'')';
	'xstring(orig(1)+dd,y(3)-4,''Init'')';
	'xstring(orig(1)+dd,y(4)-4,''BInit'')';
	'[x,y,typ]=standard_outputs(o) ';
	'xstring(orig(1)+sz(1)-dd,y(1)-4,''S1'')';
	'xstringb(orig(1)+2*dd,orig(2),''BascR'',sz(1)-4*dd,sz(2),''fill'');']
    ok =%t
  else
    model.rpar=num
    [ok,init,b_init,exprs]=getvalue('Set Pulse parameter',..
	    ['Init (0=false 1=true)';'B_init (0=false 1=true)'],list('vec',1,'vec',1),exprs)
    if ~ok then break,end
    if and(init<>[0 1]) then message('Init must be only 0 or 1'),ok=%f;end
    if and(b_init<>[0 1]) then message('B_init must be only 0 or 1'),ok=%f;end
    if ok then
       in=[1 1;1 1];it=[9 9]
       model.ipar=[init;b_init]
       graphics.gr_i=['[x,y,typ]=standard_inputs(o) ';
	'dd=sz(1)/10,de=9*sz(1)/10';
	'xstring(orig(1)+dd,y(1)-4,''S'')';
	'xstring(orig(1)+dd,y(2)-4,''R'')';
	'[x,y,typ]=standard_outputs(o) ';
	'xstring(orig(1)+sz(1)-dd,y(1)-4,''S1'')';
	'xstringb(orig(1)+2*dd,orig(2),''BascR'',sz(1)-4*dd,sz(2),''fill'');']
     end
  end
  if ok then
      out=[1 1];ot=9
      model.odstate=list(int32(0))
     [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),[],[])
     graphics.exprs=exprs
     x.model=model;x.graphics=graphics;
     arg1=x;
     break;
   end
  end
case 'define' then
  model=scicos_model()
  junction_name='bascr';
  funtyp=4;
  model.sim=list(junction_name,funtyp)
  model.in=[1;1;1;1]
  model.in2=[1;1;1;1]
  model.intyp=[9 9 9 9]
  model.out=1
  model.out2=1
  model.outtyp=9
  model.evtin=[]
  model.evtout=[]
  model.state=[]
  model.dstate=[]
  model.odstate=list(int32(0))
  model.rpar=2
  model.ipar=[]
  model.blocktype='c' 
  model.firing=[]
  model.dep_ut=[%t %f]
  exprs=[sci2exp(0);sci2exp(0)];
  gr_i=['[x,y,typ]=standard_inputs(o) ';
	'dd=sz(1)/10,de=9*sz(1)/10';
	'xstring(orig(1)+dd,y(1)-4,''S'')';
	'xstring(orig(1)+dd,y(2)-4,''R'')';
	'xstring(orig(1)+dd,y(3)-4,''Init'')';
	'xstring(orig(1)+dd,y(4)-4,''BInit'')';
	'[x,y,typ]=standard_outputs(o) ';
	'xstring(orig(1)+sz(1)-dd,y(1)-4,''S1'')';
	'xstringb(orig(1)+2*dd,orig(2),''BascR'',sz(1)-4*dd,sz(2),''fill'');']
  x=standard_define([5 4],model,exprs,gr_i)
end
endfunction

