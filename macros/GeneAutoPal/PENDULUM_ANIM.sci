function [x,y,typ]=PENDULUM_ANIM(job,arg1,arg2)
// Animation of the cart-pendulum problem
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(o)
   case 'getoutputs' then
    x=[];y=[];typ=[];
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1;
    graphics=arg1.graphics;exprs=graphics.exprs
    model=arg1.model;dstate=model.dstate
    while %t do
      [ok,plen,csiz,phi,xmin,xmax,ymin,ymax,exprs]=getvalue(..
        'Set Scope parameters',..
        ['pendulum length';'cart size (square side)';'slope';
        'Xmin';'Xmax';  'Ymin'; 'Ymax'; ],..
        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs)
      if ~ok then break,end
      mess=[]
      if plen<=0|csiz<=0 then
        mess=[mess;'Pendulum lenght and cart size must be positive.';' ']
        ok=%f
      end
      if ymin>=ymax then
        mess=[mess;'Ymax must be greater than Ymin';' ']
        ok=%f
      end
      if xmin>=xmax then
        mess=[mess;'Xmax must be greater than Xmin';' ']
        ok=%f
      end
      if ~ok then
        message(mess)
      else
        rpar=[plen;csiz;phi;xmin;xmax;ymin;ymax]
        model.rpar=rpar;
        graphics.exprs=exprs;
        x.graphics=graphics;x.model=model
        break
      end
    end
   case 'define' then
    plen=2; csiz=2; phi=0;
    xmin=-5;xmax=5;ymin=-5;ymax=5

    model=scicos_model()
    model.sim=list('anim_pen',5)
    model.in=[1;1]
    model.evtin=1
    model.dstate=0
    model.rpar=[plen;csiz;phi;xmin;xmax;ymin;ymax]
    model.blocktype='d'
    model.dep_ut=[%f %f]
    
    exprs=string(model.rpar)
    gr_i=['thick=xget(''thickness'');xset(''thickness'',2);';
          'xx=orig(1)+sz(1)*[.4 .6 .6 .4 .4]'
          'yy=orig(2)+sz(2)*[.2 .2 .4 .4 .2]'
          'xpoly(xx,yy,''lines'')'
          'xx=orig(1)+sz(1)*[.5 .6]'
          'yy=orig(2)+sz(2)*[.4 .8]'
          'xpoly(xx,yy)'
          'xset(''thickness'',thick);']
    x=standard_define([3 3],model,exprs,gr_i)
  end
endfunction


function [blocks] = anim_pen(blocks,flag)
  win=20000+curblock()
  xset('window',win)
  set('figure_style','new')
  H=gcf()
  
  xold=blocks.z
  rpar=blocks.rpar
  plen=rpar(1);csiz=rpar(2);phi=rpar(3);
  
  if flag==4 then 
    clf(H)
    H.pixmap='on'
    Axe=gca()
    Axe.data_bounds=rpar(4:7)
    Axe.isoview='on'
    S=[cos(phi),-sin(phi);sin(phi),cos(phi)]
    XY=S*[rpar(4),rpar(5);-csiz/2,-csiz/2]
    xsegs(XY(1,:),XY(2,:))
    x=0;theta=0;
    x1=x-csiz/2;x2=x+csiz/2;y1=-csiz/2;y2=csiz/2
    XY=S*[x1 x2 x2 x1 x1;y1,y1,y2,y2,y1]
    xpoly(XY(1,:),XY(2,:),"lines",1)
    XY=S*[x,x+plen*sin(theta);0,0+plen*cos(theta)]
    xsegs(XY(1,:),XY(2,:))
    
  elseif flag==2 then
    Axe=gca()
    x=blocks.inptr(1)(1)
    theta=blocks.inptr(2)(1)
    XY=Axe.children(2).data'+..
       [cos(phi)*(x-xold);sin(phi)*(x-xold)]*ones(1,5)
    Axe.children(2).data=XY'
    x1=x*cos(phi);y1=x*sin(phi)
    XY=[x1,x1+plen*sin(theta);y1,y1+plen*cos(theta)]
    Axe.children(1).data=XY'
    blocks.z=x
    show_pixmap()
  end
endfunction
