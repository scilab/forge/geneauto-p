function [x,y,typ]=Terminator(job,arg1,arg2)
// Copyright INRIA
x=[];y=[];typ=[];
select job
 case 'plot' then
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  x=[];y=[];typ=[];
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1;
case 'define' then
  C=[0]
  model=scicos_model()
  model.sim='lsplit'
  model.in=-1
  model.out=[]
  model.in2=-2
  model.out2=[]
  model.outtyp=1
  model.intyp=-1
  model.rpar=[]
  model.opar=list();
  model.blocktype='d'
  model.dep_ut=[%f %f]
  exprs=[]
  gr_i=['x=orig(1)*ones(7,1)+sz(1)*[1/4;7/8;7/8;1/8;7/8;14/16;1/4];';
  	  'y=orig(2)*ones(7,1)+sz(2)*[1/4;1/4;1/2;1/2;1/2;3/4;3/4];';
	  'xpolys(x,y);']
  x=standard_define([2 2],model,exprs,gr_i)
end
endfunction
