// ====================================================================
// Allan CORNET
// DIGITEO 2009-2011
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
function cleanmacros()
  macros_path = get_absolute_file_path('cleanmacros.sce');

  subdirs = ["cosf_generator" "GeneAutoPal" "xml_generator"];
  directories = pathconvert(macros_path+"/"+subdirs,%F);
  for i=1:size(directories,"*") do
    if ~isdir(directories(i)) then
        error(msprintf(gettext("%s: The directory ''%s'' doesn''t exist or is not read accessible.\n"),"buildmacros",directories(i)));
    end
  end
  tbx_builder(pathconvert(directories+"/cleanmacros.sce",%F));
endfunction

cleanmacros();
clear cleanmacros; // remove cleanmacros on stack
// ====================================================================
