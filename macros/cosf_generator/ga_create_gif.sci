function ga_create_gif(TXT,path)
  load SCI/modules/scicos/macros/lib;
  exec(loadpallibs,-1) 
  %scicos_debug_gr=%f,%zoom=1.5;Select=[];swap_handles=[];
  rgb=[.3,.4,.5] ; // used for transparent
  options=default_options();
  curdir=getcwd()
  chdir(path)
  R=60
  for i=1:size(TXT,1)
    [xpath,name,ext]=splitfilepath(TXT(i))
    execstr('o='+name+'(''define'')');
    sz=20*o.graphics.sz;
//    if sz(1)>2*sz(2)
//      sz(2)=sz(1)/2;
//    else
//      sz(1)=2*sz(2);
//    end
    o.graphics.sz=sz;
    //R=(max(sz)+15)  // add margin for ports
    orig=(R*[1,1]-sz)/2+[5 0];
    o.graphics.orig=orig;
    o.graphics.id="";
    scs_m=scicos_diagram();
    scs_m.objs(1)=o;
    [w,h]=do_export(scs_m,name,0);
    unix('convert -size '+sci2exp(w+50)+'x'+sci2exp(h)+' -rotate 90 '+name+'.eps '+name+'.gif');
    unix('rm '+name+'.eps');
    unix('convert -resize 100x50 '+name+'.gif '+name+'.gif')
    r=string(100*rgb(1));g=string(100*rgb(2));b=string(100*rgb(3));
    unix('convert -transparent '"rgb(76,102,127)'" '+name+'.gif ...
	 '+name+'.gif');
   end
   chdir(curdir)
endfunction

