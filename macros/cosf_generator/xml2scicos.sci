// Convert from the Gene-Auto System Model XML to Scicos

// Authors : Fady Nassif, Serge Steer. INRIA. 2007-2009. GPL Version 3
// Contributors : Daniel Tuulik, Andres Toom. IB Krates. 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

// TODO: Use the XML reader from Scilab 5.4.0

function [ok,scs_m,SBIO,SBInfo,atomflg]=xml2scicos(xmlfilename,mfilename,flag)
// the ifthenelse block and the atomic must be studied.
typ=[];lmax=80;link_matrix=[];tit='';BlocksInfo=[];SBIO=[];SBInfo=[];
sample_mat=[];counter=0;
atomflg=%f
ierr=0;par_name=[];par_val=[]
global newline;
[lhs,rhs]=argn(0)
if rhs<3 then
  txt=mgetl(xmlfilename);
  newline=0;
  if rhs<2 then
    cntxt='';
  else
    cntxt=mgetl(mfilename)
    cntxt=strsubst(cntxt,'%','//')
  end
  [%scicos_context,ierr]=script2var(sci2exp(cntxt),struct())
end
scs_m=scicos_diagram();
scs_m.props=scicos_params();
if rhs==2 then
  scs_m.props.context=sci2exp(cntxt)
end
while typ(1)<>'</SystemBlock>' do
  if typ(1)=='</GASystemModel>'  then break;end
  if typ(1)=='<SystemBlock' then
    for i=1:size(typ,'*')
      ttyp=tokens(typ(i),'=')
      if ttyp(1)=='name' then
	tit=ttyp(2);
      end
    end
    counter=counter+1;
    scs_m.objs(counter)=scicos_block()
    [ok,DefinedBlock,ga_name,par_name,par_val]=GetBlock(typ);
    if ~ok then scs_m=scicos_diagram();return;end
    typ=get_typ(txt);
    indst=find(par_name=="SampleTime");
    if indst <> [] then 
      SBSampleTime=evstr(par_val(indst));
    else
      SBSampleTime=-1;
    end
    [ok,scs_m1,SBIO,SBInfo,atomflg]=xml2scicos('','',%t);
    // This part is done here because in the second phase of the xml the action port disapears
    // So to add it we have to see if the block is an if block 
    tit=strsubst(tit,"""","");
    tit_typ=tokens(tit)
    if SBIO<>[] then 
    NbreofVirtualEInPort=size(vectorfind(SBIO(:,[3,4]),[1 1],'r'),'*')
    if SBSampleTime<>-1 
      if NbreofVirtualEInPort==0 then
	NbreofVirtualEInPort=1;
	sample_mat=[counter SBSampleTime];
      else
	message('Cannot have a Sample Time for a controled block');
	return;
      end
    end
    if NbreofVirtualEInPort<>0 then
      rect=GetBounds(scs_m1);
      DefBlock=CLKINV_f('define')
      sz=[40 40];
      orig=[rect(1)-sz(1) rect(4)+sz(2)]
      DefBlock.graphics.orig=[orig(1) orig(2)+sz(2)/2]
      DefBlock.graphics.sz=[sz(1) sz(2)/2]
      DefBlock.graphics.peout=lstsize(scs_m1.objs)+3;
      DefBlock.graphics.flip=%t
      scs_m1.objs($+1)=DefBlock
      //Adding the CLKO block
      DefBlock=VirtualCLK0('define')
      DefBlock.graphics.orig=[orig(1) orig(2)]
      DefBlock.graphics.sz=[sz(1) sz(2)/2]
      DefBlock.graphics.pein=lstsize(scs_m1.objs)+2;
      scs_m1.objs($+1)=DefBlock
      //Adding the link between the two
      DefLink=scicos_link();
      DefLink.ct=[5 -1]
      DefLink.from=[lstsize(scs_m1.objs)-1 1 0]
      DefLink.to=[lstsize(scs_m1.objs) 1 1]
      [x1,y1]=getoutputs(scs_m1.objs($-1))
      [x2,y2]=getinputs(scs_m1.objs($))
      DefLink.xx=[x1;x2]
      DefLink.yy=[y1;y2]
      scs_m1.objs($+1)=DefLink
    end
    end
    tit=strsubst(tit,"\n","_");
    tit=strsubst(tit," ","_");
    scs_m1.props.title=tit 
    if ~ok then scs_m=scicos_diagram();return;end
    scs_m1=do_beautify(scs_m1)
    DefinedBlock.model.rpar=scs_m1
    //[ok,BlockInformation,DefinedBlock,sample_mat]=GetBlockInformation(typ,DefinedBlock,sample_mat,par_name,par_val)  // BlockInformation--> NbreofBlock NbreofPort IdofPort XposofBlock YposofBlock PortType IO
    //if ~ok then scs_m=scicos_diagram();return;end
    DefinedBlock.graphics.orig=SBInfo(:,[1 2])
    DefinedBlock.graphics.sz=SBInfo(:,[3 4])
    if SBIO<>[] then
      I='E'
      // must define the graphics and the model nbre input ...
      NbreofVirtualInPort=size(vectorfind(SBIO(:,[3,4]),[1 0],'r'),'*')
      NbreofVirtualOutPort=size(vectorfind(SBIO(:,[3,4]),[0 0],'r'),'*')
      //NbreofVirtualEInPort=size(vectorfind(SBIO(:,[3,4]),[1 1],'r'),'*')
      DefinedBlock.graphics.pin=zeros(NbreofVirtualInPort,1)
      DefinedBlock.graphics.pout=zeros(NbreofVirtualOutPort,1)
      DefinedBlock.graphics.pein=zeros(NbreofVirtualEInPort,1)
      DefinedBlock.graphics.in_implicit=I(ones(NbreofVirtualInPort,1))
      DefinedBlock.graphics.out_implicit=I(ones(NbreofVirtualOutPort,1))
      DefinedBlock.model.in=-ones(NbreofVirtualInPort,1)
      DefinedBlock.model.in2=-2*ones(NbreofVirtualInPort,1)
      DefinedBlock.model.intyp=-ones(NbreofVirtualInPort,1)
      DefinedBlock.model.out=-ones(NbreofVirtualOutPort,1)
      DefinedBlock.model.out2=-2*ones(NbreofVirtualOutPort,1)
      DefinedBlock.model.outtyp=-ones(NbreofVirtualInPort,1)
      DefinedBlock.model.evtin=ones(NbreofVirtualEInPort,1)
      [x,y,typ]=getinputs(DefinedBlock)
      nb=size(find(typ==1),'*');
      nbus=size(find(typ==2),'*');
      PortPosition=[];BlockInformation=[];
      if x<>[] then PortPosition=[PortPosition;[x(:) y(:) [1:nb+nbus 1:size(x,'*')-nb-nbus]' ones(size(x,'*'),1) (typ(:)<0)]];end
      [x,y,typ]=getoutputs(DefinedBlock)
      nb=size(find(typ==1),'*');
      nbus=size(find(typ==2),'*');
      if x<>[] then PortPosition=[PortPosition;[x(:) y(:) [1:nb+nbus 1:size(x,'*')-nb-nbus]' zeros(size(x,'*'),1) (typ(:)<0)]];end
      vect_to_ident=PortPosition(:,[3:5])
      for i =1:size(vect_to_ident,1)
	ind=vectorfind(SBIO(:,[1 3 4]),vect_to_ident(i,:),'r');
	if ind<>[] then BlockInformation=[BlockInformation;[counter SBIO(ind,[1:2]) PortPosition(i,[1:2,5,4])]];end
      end
    BlocksInfo=[BlocksInfo;BlockInformation];
    end
    if atomflg==%t then DefinedBlock.model.sim(1)='asuper';end
    DefinedBlock.model.rpar=scs_m1
    if rhs<3 then
      scs_m=scs_m1;
    else
      scs_m.objs(counter)=DefinedBlock
    //if atomflg==%t then 
      //[DefinedBlock,needcompile,ok]=do_CreateAtomic(DefinedBlock,counter,scs_m);  // to be adjusted to take care of sample time inside atomic block
      //if ~ok then message('Error in Creating Atomic');return ;end
      //scs_m = update_redraw_obj(scs_m,list('objs',counter),DefinedBlock);
    //end
    end
    typ=txt(newline)
    SBIO=[];SBInfo=[];
  elseif or(typ(1)==['<SourceBlock';'<SinkBlock';'<CombinatorialBlock';'<SequentialBlock';'<ControlBlock';'<GenericBlock']) then
    [ok,DefinedBlock,ga_name,par_name,par_val]=GetBlock(typ);
    if ~ok then scs_m=scicos_diagram();return;end
    if or(ga_name==['ActionPort']) then //NbreofVirtualEInPort=NbreofVirtualEInPort+1;
     disp(ga_name); pause
//      counter=counter+1;
//      [ok,BlockInformation,DefinedBlock,sample_mat]=GetBlockInformation(typ,DefinedBlock,sample_mat,par_name,par_val,counter)
//      orig=DefinedBlock.graphics.orig;
//      sz=DefinedBlock.graphics.sz;
//      DefinedBlock.graphics.orig=[orig(1) orig(2)+sz(2)/2]
//      DefinedBlock.graphics.sz=[sz(1) sz(2)/2]
//      DefinedBlock.graphics.peout=counter+2
//      DefinedBlock.graphics.flip=%t
//      scs_m.objs(counter)=DefinedBlock
//      //Adding the CLKO block
//      counter=counter+1;
//      DefinedBlock=VirtualCLK0('define')
//      DefinedBlock.graphics.orig=[orig(1) orig(2)]
//      DefinedBlock.graphics.sz=[sz(1) sz(2)/2]
//      DefinedBlock.graphics.pein=counter+1;
//      scs_m.objs(counter)=DefinedBlock
//      //Adding the link between the two
//      counter=counter+1;
//      DefinedLink=scicos_link();
//      DefinedLink.ct=[5 -1]
//      DefinedLink.from=[counter-2 1 0]
//      DefinedLink.to=[counter-1 1 1]
//      [x1,y1]=getoutputs(scs_m.objs(counter-2))
//      [x2,y2]=getinputs(scs_m.objs(counter-1))
//      DefinedLink.xx=[x1;x2]
//      DefinedLink.yy=[y1;y2]
//      scs_m.objs(counter)=DefinedLink
    else
      counter=counter+1;
      scs_m.objs(counter)=scicos_block()
      //if ga_name=='Inport' then NbreofVirtualInPort=NbreofVirtualInPort+1;
      //elseif ga_name=='Outport' then NbreofVirtualOutPort=NbreofVirtualOutPort+1;
      //elseif or(ga_name==['For']) then NbreofVirtualEInPort=NbreofVirtualEInPort+1;continue;
      //end
      typ=get_typ(txt);
      [ok,BlockInformation,DefinedBlock,sample_mat]=GetBlockInformation(typ,DefinedBlock,sample_mat,par_name,par_val,counter)  // BlockInformation--> NbreofBlock NbreofPort IdofPort XposofBlock YposofBlock PortType IO
      if ~ok then scs_m=scicos_diagram();return;end
      BlocksInfo=[BlocksInfo;BlockInformation];
      scs_m.objs(counter)=DefinedBlock
    end
  elseif or(typ(1)==['<Signal']) then
    counter=counter+1;
    while typ(1)<>'</Signal>' do
      typ=get_typ(txt)
      if typ(1)=='<srcPort' then
        ttyp=tokens(typ(2),'>')     //must be modified in the general case
        tttyp=tokens(ttyp(2),'<')   // must be modified in the general case
        info=BlocksInfo(find(BlocksInfo(:,3)==evstr(tttyp(1))),:)
        fromBlk=info(1);fromPrt=info(2);typePrt=info(6);
      elseif typ(1)=='<dstPort' then
        ttyp=tokens(typ(2),'>')     //must be modified in the general case
        tttyp=tokens(ttyp(2),'<')   // must be modified in the general case
        info=BlocksInfo(find(BlocksInfo(:,3)==evstr(tttyp(1))),:)
        toBlk=info(1);toPrt=info(2);
      end
    end
    if link_matrix<>[] then ind=vectorfind(link_matrix(:,2:3),[fromBlk fromPrt],'r');
    else ind=[];
    end
    if ind==[] then
      link_matrix=[link_matrix;..
	      [counter,fromBlk,fromPrt,toBlk,toPrt,typePrt]];
    else
      ierr=execstr('DefinedBlock=SPLIT_f('"define'")','errcatch')
      //scicos_graphics
      XYOutputsPorts=BlocksInfo(vectorfind(BlocksInfo(:,[1:2,6:7]),[fromBlk fromPrt 0 0],'r'),[4:5]) // X and Y position of port fromPrt of block fromBlk
      DefinedBlock.graphics.orig=[XYOutputsPorts(1)+5 XYOutputsPorts(2)]
      scs_m.objs(counter)=DefinedBlock
      link_matrix=[link_matrix;..
	      [counter+1 counter 1 link_matrix(ind,4) link_matrix(ind,5) 0];..
	      [counter+2 counter 2 toBlk toPrt 0]]
      link_matrix(ind,4:5)=[counter 1]
      counter=counter+2;
    end
  elseif or(typ(1)==['<InDataPort','<OutDataPort']) then
    execstr(xml_subst(typ(2:$-1,1)));
    IO=modulo(find(['<InDataPort';'<OutDataPort']==typ(1)),2)  //input=1 output=0;
    porttype=0; // data=0 control=1;
    SBIO=[SBIO;[evstr(portNumber) evstr(id) IO porttype]]
  elseif typ(1)=='<InControlPort' then
    execstr(xml_subst(typ(2:$-1,1)));
    SBIO=[SBIO;[1 evstr(id) 1 1]]
  elseif typ(1)=='<DiagramInfo' then
      execstr(xml_subst(typ(2:$-1,1)));
    if exists('sizeX') then SBInfo=[evstr(positionX) 450-evstr(positionY) evstr(sizeX) evstr(sizeY)]
    else SBInfo=[evstr(positionX) 450-evstr(positionY) 60 60]
    end
  elseif typ(1)=='<BlockParameter' then
    execstr(xml_subst(typ(2:$-1,1)));
    par_name=[par_name;name];
  elseif typ(1)=='<StringValue' then
    execstr(xml_subst(typ(2:$-1)));
    par_val=[par_val;xml_subst(value)]
    ind=find(par_name=='AtomicSubSystem')
    if ind<>[] then
      if par_val(ind)=='on' then atomflg=%t;end
    end
  end
  typ=get_typ(txt);
end
for i=1:size(link_matrix,1)
  DefinedLink=scicos_link();
  [x,y,typ]=getoutputs(scs_m.objs(link_matrix(i,2))) //get outputs of the source block;
  typePrt=link_matrix(i,$)
  if typePrt==0 then // data
    ind=find(typ(:)>0)
  else
    ind=find(typ(:)<0)
  end
  x=x(ind(:));y=y(ind(:));typ=typ(ind(:));
  DefinedLink.xx(1)=x(link_matrix(i,3)) // Xpostion of port nbre fromPort.
  DefinedLink.yy(1)=y(link_matrix(i,3)) // Ypostion of port nbre fromPort.
  if typ(link_matrix(i,3))==1 then 
    DefinedLink.ct=[1 1]; // regular link
    scs_m.objs(link_matrix(i,2)).graphics.pout(link_matrix(i,3))=link_matrix(i,1)
  elseif typ(link_matrix(i,3))==-1 then 
    DefinedLink.ct=[5 -1]; //event link
    scs_m.objs(link_matrix(i,2)).graphics.peout(link_matrix(i,3))=link_matrix(i,1)
  elseif typ(link_matrix(i,3))==2 then 
    DefinedLink.ct=[2 2]; //buses input
    scs_m.objs(link_matrix(i,2)).graphics.pout(link_matrix(i,3))=link_matrix(i,1)
  end
  DefinedLink.from=[link_matrix(i,2) link_matrix(i,3) 0];
  DefinedLink.to=[link_matrix(i,4) link_matrix(i,5) 1];
  [x,y,typ]=getinputs(scs_m.objs(link_matrix(i,4)))  //get inputs of the destination port
  if typePrt==0 then // data
    ind=find(typ(:)>0)
  else
    ind=find(typ(:)<0)
  end
  x=x(ind(:));y=y(ind(:));typ=typ(ind(:));  
  DefinedLink.xx(2)=x(link_matrix(i,5)) // Xpostion of port nbre fromPort.
  DefinedLink.yy(2)=y(link_matrix(i,5)) // Ypostion of port nbre fromPort.
  if or(typ(link_matrix(i,5))==[1;2]) then 
    scs_m.objs(link_matrix(i,4)).graphics.pin(link_matrix(i,5))=link_matrix(i,1)
  elseif typ(link_matrix(i,5))==-1 then
    scs_m.objs(link_matrix(i,4)).graphics.pein(link_matrix(i,5))=link_matrix(i,1)
  end
  scs_m.objs(link_matrix(i,1))=DefinedLink;
end
// Adding the SampleCLK
n=size(sample_mat,1)
for i=1:n
  XX=scs_m.objs(sample_mat(i,1))
  if XX.graphics.pein<>[] then
    bk = SampleCLK('define');
    [posx,posy] = getinputports(XX);
    posx=posx($);posy=posy($);
    sz=XX.graphics.sz;
    teta = XX.graphics.theta
    pos  = rotate([posx;posy],teta*%pi/180, ...
	[XX.graphics.orig(1)+sz(1)/2,...
	    XX.graphics.orig(2)+sz(2)/2]) ;
    posx = pos(1); posy = pos(2);
    bk.graphics.orig = [posx posy]+[-30 20]
    bk.graphics.sz = [60 40]
    bk.graphics.exprs = [sci2exp(sample_mat(i,2));string(0)]
    bk.model.rpar = [sample_mat(i,2),0];
    bk.graphics.peout = counter+2
    counter=counter+1
    scs_m.objs(counter) = bk;
    [posx2,posy2] = getoutputports(bk);
    lnk    = scicos_link();
    lnk.xx = [posx2;posx];
    lnk.yy = [posy2;posy];
    lnk.ct = [5 -1]
    lnk.from = [counter 1 0]
    lnk.to = [sample_mat(i,1) 1 1]
    counter=counter+1
    scs_m.objs(counter) = lnk;
    scs_m.objs(sample_mat(i,1)).graphics.pein=counter;
  end
end
//typ=get_typ(txt)

endfunction


function [num,ok]=tran_type(name)
ok=%t
if name=='double' then num=1;
elseif name=='ZZZ' then num=2;
elseif name=='int32' then num=3;
elseif name=='int16' then num=4;
elseif name=='int8' then num=5;
elseif name=='uint32' then num=6;
elseif name=='uint16' then num=7;
elseif name=='uint8' then num=8;
else message('An error occured while translating check the type'); ok=%f;return;
end
endfunction

function t=read_new_line(txt)
  global newline;
  newline=newline+1;
  t=txt(newline);
endfunction

function typ=get_typ(txt)
  t=read_new_line(txt);
  t=stripblanks(t,%t)
  ttyp=tokens(t);
  if or(ttyp(1)==['<SourceBlock';'<SinkBlock';'<CombinatorialBlock';'<SequentialBlock';'<ControlBlock';'<SystemBlock';'<GenericBlock']) then
    ind1=strindex(t,'directFeedThrough=');if ind1<>[] then ind1=ind1-1;end
    ind2=strindex(t,'id=');if ind2<>[] then ind2=ind2(1)-1;end
    ind3=strindex(t,'isVirtual=');if ind3<>[] then ind3=ind3-1;end
    ind4=strindex(t,'name=');if ind4<>[] then ind4=ind4(1)-1;end
    ind5=strindex(t,'sampleTime=');if ind5<>[] then ind5=ind5-1;end
    ind6=strindex(t,'type=');if ind6<>[] then ind6=ind6-1;end
    ind7=strindex(t,'>');if ind7<>[] then ind7=ind7-1;end
    ind8=strindex(t,'originalFullName=');if ind8<>[] then ind8=ind8-1;end
    ind9=strindex(t,'assignedPriority=');if ind9<>[] then ind9=ind9-1;end
    ind10=strindex(t,'assignedPrioritySource=');if ind10<>[] then ind10=ind10-1;end
    ind11=strindex(t,'fcnname=');if ind11<>[] then ind11=ind11-1;end
    ind12=strindex(t,'executionOrder');if ind12<>[] then ind12=ind12-1;end
    ind=gsort([ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 ind9 ind10 ind11 ind12],'g','i')
    typ= strsplit(t,ind)
  elseif or(ttyp(1)==['<InDataPort';'<OutDataPort';'<OutControlPort';'<InControlPort']) then
    ind1=strindex(t,'periodicSampleTime=');if ind1<>[] then ind1=ind1-1;end
    ind2=strindex(t,'id=');if ind2<>[] then ind2=ind2(1)-1;end
    ind4=strindex(t,'resetStates=');if ind4<>[] then ind4=ind4-1;end
    ind3=strindex(t,'/>');if ind3<>[] then ind3=ind3-1;end
    ind5=strindex(t,'originalFullName=');if ind5<>[] then ind5=ind5-1;end
    ind6=strindex(t,'relatedToInportBlock=');if ind6<>[] then ind6=ind6-1;end
    ind7=strindex(t,'portNumber=');if ind7<>[] then ind7=ind7-1;end
    ind8=strindex(t,'name=');if ind8<>[] then ind8=ind8(1)-1;end
    if (ind8<>[] & ind5 <>[] & ind5<ind8) then ind8=[];end
    ind9=[];
    if ind3==[] then ind9=strindex(t,'>');if ind9<>[] then ind9=ind9-1;end;end
    ind=gsort([ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8 ind9],'g','i')
    typ= strsplit(t,ind);
  elseif ttyp(1)=='<DiagramInfo' then
    ind1=strindex(t,'positionX=');if ind1<>[] then ind1=ind1-1;end
    ind2=strindex(t,'positionY=');if ind2<>[] then ind2=ind2-1;end
    ind3=strindex(t,'sizeX=');if ind3<>[] then ind3=ind3-1;end
    ind4=strindex(t,'sizeY=');if ind4<>[] then ind4=ind4-1;end
    ind5=strindex(t,'/>');if ind5<>[] then ind5=ind5-1;end
    ind6=strindex(t,'originalFullName=');if ind6<>[] then ind6=ind6-1;end
    ind=gsort([ind1 ind2 ind3 ind4 ind5 ind6],'g','i')
    typ= strsplit(t,ind)
  elseif ttyp(1)=='<BlockParameter' then
    ind1=strindex(t,'name=');if ind1<>[] then ind1=ind1(1)-1;end
    ind2=strindex(t,'id=');if ind2<>[] then ind2=ind2(1)-1;end
    ind3=strindex(t,'>');if ind3<>[] then ind3=ind3-1;end
    ind4=strindex(t,'originalFullName=');if ind4<>[] then ind4=ind4-1;end
    ind=gsort([ind1 ind2 ind3 ind4],'g','i')
    typ= strsplit(t,ind)
  elseif ttyp(1)=='<StringValue' then
    ind1=strindex(t,'value=');if ind1<>[] then ind1=ind1-1;end
    ind2=strindex(t,'id=');if ind2<>[] then ind2=ind2(1)-1;end
    ind3=strindex(t,'/>');if ind3<>[] then ind3=ind3-1;end
    ind4=strindex(t,'originalFullName=');if ind4<>[] then ind4=ind4-1;end
    ind=gsort([ind1 ind2 ind3 ind4],'g','i')
    typ= strsplit(t,ind)
  elseif ttyp(1)=='<IntegerExpression' then
    ind1=strindex(t,'hexValue=');		if ind1<>[] then ind1=ind1-1;end
    ind2=strindex(t,'id=');		 		if ind2<>[] then ind2=ind2(1)-1;end
    ind3=strindex(t,'>');		 		if ind3<>[] then ind3=ind3-1;end
    ind4=strindex(t,'litValue=');		if ind4<>[] then ind4=ind4-1;end
    ind5=strindex(t,'exponent=');		if ind5<>[] then ind5=ind5-1;end
    ind6=strindex(t,'integerPart=');	if ind6<>[] then ind6=ind6-1;end
    ind7=strindex(t,'isNegative=');		if ind7<>[] then ind7=ind7-1;end
    ind8=strindex(t,'scientificValue=');if ind8<>[] then ind8=ind8-1;end
    ind=gsort([ind1 ind2 ind3 ind4 ind5 ind6 ind7 ind8],'g','i')
    typ= strsplit(t,ind)
  else
    typ=ttyp;
  end
  typ=stripblanks(typ,%t)
  //typ=tokens(typ(1),'>');
endfunction

function [ok,DefinedBlock,ga_name,par_name,par_val]=GetBlock(typ)
  ok=%t;DefinedBlock=scicos_block();par_name=[];par_val=[];ga_name='';depu=%t;
  for i=1:size(typ,'*')
    ttyp=tokens(typ(i),'=');
    if ttyp(1)=='directFeedThrough' then
      if ttyp(2)==sci2exp('true') then depu=%t; else depu=%f; end
    elseif ttyp(1)=='type' then
      tttyp=tokens(ttyp(2),'>')
      ga_name=evstr(tttyp(1))
      //disp(ga_name);
      //if ga_name=='ActionPort' then ok=%t;return;end
      sci_name=translatename(ga_name)
      //disp(sci_name);
      ierr=execstr('DefinedBlock='+sci_name+'('"define'")','errcatch')
      if ierr then message('Block is not defined. Please check the block '+sci_name);ok=%f;return; end
    elseif ttyp(1)=='sampleTime' then
      par_name='SampleTime';
      par_val=evstr(ttyp(2));
    end
  end
  DefinedBlock.model.dep_ut(1)=depu;
endfunction

function [ok,BlockInformation,DefinedBlock,sample_mat]=GetBlockInformation(typ,DefinedBlock,sample_mat,par_name,par_val,counter)
    BlockInformation=[];PortInformation=[];PortPosition=[];ok=%t;
    
    // Process the block's parameters
    while and(typ(1)<>['</SourceBlock>';'</SinkBlock>';'</CombinatorialBlock>';'</SequentialBlock>';'</ControlBlock>';'</SystemBlock>';'</GenericBlock>']) do
      if or(typ(1)==['<InDataPort';'<OutDataPort']) then
        execstr(xml_subst(typ(2:$-1,1)));
        IO=modulo(find(['<InDataPort';'<OutDataPort']==typ(1)),2)  //input=1 output=0;
        porttype=0; // data=0 control=1
        PortInformation=[PortInformation;[counter evstr(portNumber) evstr(id) IO porttype]]
      elseif or(typ(1)==['<InControlPort';'<OutControlPort']) then
        execstr(xml_subst(typ(2:$-1,1)));
		IO=modulo(find(['<InControlPort';'<OutControlPort']==typ(1)),2)
		if typ(1)=='<InControlPort' then
		  PortInformation=[PortInformation;[counter 1 evstr(id) IO 1]]
		else
		  PortInformation=[PortInformation;[counter evstr(portNumber) evstr(id) IO 1]]
		end
      elseif typ(1)=='<DiagramInfo' then
        execstr(xml_subst(typ(2:$-1,1)));
        DefinedBlock.graphics.orig=[evstr(positionX) 450-evstr(positionY)];
		if exists('sizeX') then DefinedBlock.graphics.sz=[evstr(sizeX) evstr(sizeY)];
		else DefinedBlock.graphics.sz=[20 20];
		end
	  elseif typ(1)=='<BlockParameter' then
	    execstr(typ(2:$-1,1));
		par_name=[par_name;name];
	  elseif typ(1)=='<StringValue' then
	    execstr(typ(2:$-1,1));
		par_val=[par_val;xml_subst(value)]
	  elseif typ(1)=='<IntegerExpression' then
	  	// extract the value field
	    execstr(typ(2:$-1,1));
		par_val=[par_val;xml_subst(litValue)]
	  elseif typ(1)=='<ExpressionValue' then
		if endwith(typ($,1),'/>') then par_val=[par_val;'[]'];end
      end
	  // Read and parse next element
	  typ=get_typ(txt)
    end
    ind=find(par_name=='SampleTime');
    if ind<>[] then 
      if evstr(par_val(ind))<>-1 then sample_mat=[sample_mat;counter evstr(par_val(ind))];end
    end
    ierr=execstr('DefinedBlock='+ga_name+'_cosf(DefinedBlock,par_name,par_val,par_val)','errcatch');
    if ierr then pause ;message('error. Please check the file '+ga_name+'_cosf');ok=%f;return; end
    [x,y,typ]=getinputs(DefinedBlock)
    nb=size(find(typ==1),'*');
    nbus=size(find(typ==2),'*');
    if x<>[] then PortPosition=[PortPosition;[x(:) y(:) [1:nb+nbus 1:size(x,'*')-nb-nbus]' ones(size(x,'*'),1) (typ(:)<0)]];end
    //DefinedBlock.graphics.pin=zeroes(nb+nbus,1)
    //DefinedBlock.graphics.pein=zeros(size(x,'*')-nb-nbus,1)
    //A='B';I='E'
    //DefinedBlock.in_implicit=[I(ones(nb,1)) A(ones(nbus,1))];
    [x,y,typ]=getoutputs(DefinedBlock)
    nb=size(find(typ==1),'*');
    nbus=size(find(typ==2),'*');
    if x<>[] then PortPosition=[PortPosition;[x(:) y(:) [1:nb+nbus 1:size(x,'*')-nb-nbus]' zeros(size(x,'*'),1) (typ(:)<0)]];end
    //DefinedBlock.graphics.pout=zeroes(nb+nbus,1)
    //DefinedBlock.graphics.peout=zeros(size(x,'*')-nb-nbus,1)
    //A='B';I='E'
    //DefinedBlock.out_implicit=[I(ones(nb,1)) A(ones(nbus,1))];
    if ga_name=='ActionPort' then
      BlockInformation=PortPosition
    else
      vect_to_ident=PortPosition(:,[3:5])
      for i =1:size(vect_to_ident,1)
	ind=vectorfind(PortInformation(:,[2 4 5]),vect_to_ident(i,:),'r');
	if ind<>[] then BlockInformation=[BlockInformation;[PortInformation(ind,[1:3]) PortPosition(i,[1:2,5,4])]];end
      end
    end
endfunction
function field=xml_subst(field)
  field=strsubst(field,'&lt;','<');
  field=strsubst(field,'&gt;','>');
  field=strsubst(field,'&quot;','""""');
  field=strsubst(field,'&apos;','''''');
  field=strsubst(field,'&amp;','&');  
endfunction

function scs_m=do_beautify(scs_m)
  for i=1:size(scs_m.objs)
    o=scs_m.objs(i);
    x=getfield(1,o);
    if x(1)=='Link' then
       //disp(i);
       o=scicos_route(o,scs_m);
       scs_m.objs(i)=o;
    end
  end
endfunction

function lk=scicos_route(lk,scs_m)

From=lk.from(1);To=lk.to(1)
delF=scs_m.objs(From).graphics.sz/2
delT=scs_m.objs(To).graphics.sz/2
if lk.ct(2)==1 then
  forig=scs_m.objs(From).graphics.orig(1)+delF(1)
  torig=scs_m.objs(To).graphics.orig(1)+delT(1)
  [lk.xx,lk.yy]=routage(lk.xx,lk.yy,forig,torig,delF(2),delT(2))
elseif lk.ct(2)==-1   then
  forig=scs_m.objs(From).graphics.orig(2)+delF(2)
  torig=scs_m.objs(To).graphics.orig(2)+delT(2)
  [lk.yy,lk.xx]=routage(lk.yy,lk.xx,forig,torig,delF(1),delT(1))
else
  return
end
endfunction

function [x,y]=routage(x,y,forig,torig,delF,delT)
  xold=[];yold=[]
  while ~(isequal(x,xold)&isequal(y,yold))
    del=3+6*rand()
    xold=x;yold=y
    if size(x,1)>2 then
      m=find(((x(1:$-2)==x(3:$))&(x(2:$-1)==x(3:$)))|..
           ((y(1:$-2)==y(3:$))&(y(2:$-1)==y(3:$))))
      if m<>[] then
       x(m+1)=[];y(m+1)=[]
      end
    end
    n=size(x,1);
    dx=x(2:$)-x(1:$-1)
    dy=y(2:$)-y(1:$-1)
    ki=find(dx.*dy<>0)
    if ki<> [] then
     I=ones(1,n);Z=zeros(2,n)
     Z(:,ki)=1
     I=[I;Z]
    
     J=matrix(cumsum(I(:)),3,n)
     xnew=[];ynew=[]
     xnew(J(1,:),1)=x
     ynew(J(1,:),1)=y
   
     xn1=(x(ki)+x(ki+1))/2;
     xn=[xn1';xn1'];xn=xn(:);
     yn=[y(ki)';y(ki+1)'];yn=yn(:);
     j=J([2,3],ki);j=j(:)
     xnew(j,1)=xn
     ynew(j,1)=yn
     x=xnew;y=ynew
    end
    if size(x,1)>2 then
      m=find(((x(1:$-2)==x(3:$))&(x(2:$-1)==x(3:$)))|..
           ((y(1:$-2)==y(3:$))&(y(2:$-1)==y(3:$))))
      if m<>[] then
       x(m+1)=[];y(m+1)=[]
      end
    end
  end
endfunction

function b=endwith (str,op)
 b=%f;
 ind=strindex(str,op);
 if ind<>[] then
   ind=ind-1;
   b=(length(str)==(ind+length(op)))
 end
endfunction
