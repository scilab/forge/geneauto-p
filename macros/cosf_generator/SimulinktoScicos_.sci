// "Simulink to Scicos" menu handler
// Calls Gene-Auto to perform conversion between the Simulink .mdl file and 
// Scicos .cos file 
  
// Authors : Daniel Tuulik, Andres Toom. IB Krates. 08 April 2011. GPL Version 3

// Copyright (C) IB Krates 2011

function SimulinktoScicos_()

  Cmenu = []; // Clear the menu command
  
  path=tk_getfile('*.mdl',Title="Choose the mdl file to convert",multip="1")
  
  if path=='' then return; end
  
  mdl2scicos(path, %t)

endfunction
