function block_define=Switch_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Criteria');
  OPER=['u2 >= Threshold','u2 > Threshold','u2 ~= 0'];
  opnum=find(OPER==par_exprs(k));
  exprs2=sci2exp(opnum-1);
  k=find(par_name=='Threshold')
  exprs3=par_exprs(k);
  exprs1=sci2exp(block_define.model.outtyp)
  block_define.graphics.exprs=[exprs1;exprs2;exprs3;'1']
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=SWITCH2_m("set",block_define);
endfunction

  
  