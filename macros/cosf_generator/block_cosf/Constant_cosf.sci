function block_define=Constant_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Value')
  block_define.graphics.exprs=par_exprs(k)
  //disp(par_exprs(k))
  getvalue=setvalue;
  %scicos_prob=%f
  block_define=CONST_m("set",block_define);
endfunction
