function block_define=Math_cosf(block_define,par_name,par_exprs,par_val)
  oper=['exp';'log';'10^u';'log10';'magnitude^2';..
         'square';'sqrt';'pow';'conj';'reciprocal';..
          'hypot';'rem';'mod';'transpose';'hermitian']
  k=find(par_name=='Operator')
  opnum=find(oper=par_exprs(k));
  block_define.model.ipar=opnum
  block_define.graphics.exprs=[sci2exp(block_define.model.outtyp);..
                               sci2exp(1)]
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=MathFunction("set",block_define);
endfunction