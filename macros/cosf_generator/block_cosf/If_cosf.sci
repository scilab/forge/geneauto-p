function block_define=If_cosf(block_define,par_name,par_exprs,par_val)
k=find(par_name=='SampleTime')
if evstr(par_exprs(k))==-1 then
  block_define.graphics.exprs(1)='0';
else
  block_define.graphics.exprs(1)='1';
end
getvalue=setvalue;
%scicos_prob=%f;
block_define=IFTHEL_f("set",block_define);
endfunction
