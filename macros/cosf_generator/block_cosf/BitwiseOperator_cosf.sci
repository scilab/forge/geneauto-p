function block_define= BitwiseOperator_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Inputs')
  block_define.graphics.exprs(1)=par_exprs(k)
  k=find(par_name=='Operator')
  operator=['AND','OR','NAND','NOR','XOR','NOT']
  opnum=find(operator==par_exprs(k))
  block_define.graphics.exprs(2)=sci2exp(opnum-1)
  block_define.graphics.exprs(3)=sci2exp(block_define.model.outtyp);
  block_define.graphics.exprs(4)=sci2exp(1);
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=LOGICAL_OP("set",block_define);
endfunction
