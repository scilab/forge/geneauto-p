function scicosfile=xml2cosf(xmlfilename,count,scicosfile)
typ=[];lmax=80;input_mat=[];output_mat=[];link_matrix=[];from_vec=[];
[lhs,rhs]=argn(0)
ierr=0;
counter=0
block_matrix=[];
if rhs<2 then
  unit=file('open',xmlfilename,'unknown')
  count=0,
  lname='scs_m'
  scicosfile=[];
else
  count=count+1
  lname='scs_m_'+string(count)
end

while typ<>'</System>' do
  t=readc_(unit)
  typ=tokens(t)
  if typ(1)=='<System' then
    props=scicos_params();
    version =get_scicos_version();
    scicosfile=[scicosfile;
		 lname+'=scicos_diagram(..';
                 '     version=""'+version+'"",..';
                 '     props=scicos_params(..']
    tit=strsplit(typ(2),[6 length(typ(2))-2]);
    tit=tit(2);
    props.title=tit
  elseif typ(1)=='<Context' then
    context=strsplit(typ(2),[7 length(typ(2))-3]);
    context=context(2);
    props.context=context
    tt=getprops(props)
    scicosfile=[scicosfile;
		 '               '+tt]
  elseif typ(1)=='<Block' then
     // name translation.
     counter=counter+1
     ga_name=strsplit(typ(2),[11 length(typ(2))-1]);
     ga_name=ga_name(2);
     disp(ga_name);
     sci_name=translatename(ga_name)
     disp(sci_name);
     // parse the number.
     block_number_link=strsplit(typ(3),[6 length(typ(3))-1]);
     block_number_link=block_number_link(2);
     block_number=sci2exp(counter);      
     ierr=execstr('block_define='+sci_name+'('"define'")','errcatch')
     if ierr then message('Block is not defined. Please check the block '+sci_name);file('close',unit);return; end
     if sci_name=='SUPER_f' then orig_sz=strsplit(typ(4),[11 length(typ(4))-2]);
     else orig_sz=strsplit(typ(4),[11 length(typ(4))-3]);
     end
     orig_sz=orig_sz(2);
     orig_sz=strsubst(orig_sz,',',' ');
     orig_sz=tokens(orig_sz);
     orig_sz=eval(orig_sz);
     block_define.graphics.orig=[orig_sz(1) 450-orig_sz(4)]
     block_define.graphics.sz=[orig_sz(3)-orig_sz(1) orig_sz(4)-orig_sz(2)];
     if sci_name=='SUPER_f' then
        pause
	scicosfile=xml2cosf(xmlfilename,count,scicosfile)
        tr=strsplit(typ(5),[8,length(typ(5))-2])
        tr=tr(2);
	tr=strsubst(tr,',',' ');
	tr=tokens(tr);
        in=[];pin=[];in2=[];intyp=[];inimp=[];
        for i=1:eval(tr(1))
	  in=[in;-1];pin=[pin 0];in2=[in2,-2];intyp=[intyp,-1];inimp=[inimp;'E']
        end
        out=[];pout=[];out2=[];outtyp=[];outimp=[];
        for i=1:eval(tr(2))
	  out=[out;-1];pout=[pout 0];out2=[out2,-2];outtyp=[intyp,-1];outimp=[outimp,'E']
        end
        block_define.graphics.pin=pin
        block_define.graphics.pout=pout
        block_define.graphics.in_implicit=inimp
        block_define.graphics.out_implicit=outimp
        block_define.model.in=in
        block_define.model.in2=in2
        block_define.model.intyp=intyp
        block_define.model.out=out
	block_define.model.out2=out2
	block_define.model.outtyp=outtyp
     else
       // finding the parameters, the input/output size and type of the block.
       par_name=[];par_exprs=[];par_val=[];
       while typ<> '</Block>' do
        t=readc_(unit);
        typ=tokens(t);
        if typ(1)=='<Parameter' then
          // parameters for expression.
	     param_name=strsplit(typ(2),[6 length(typ(2))-1]);
	     param_name=param_name(2);
	     par_name=[par_name;param_name];
	     param_exprs=strsplit(typ(4),[7 length(typ(4))-3]);
	     param_exprs=param_exprs(2);
	     par_exprs=[par_exprs;param_exprs];
	     param_val=strsplit(typ(3),[7 length(typ(3))-1]);
	     param_val=param_val(2);
	     par_val=[par_val;param_val];
        elseif typ(1)=='<Port' then
          // port size and type.
	     port_name=strsplit(typ(2),[6 length(typ(2))-1]);
	     port_name=port_name(2);
	     port_number=strsplit(typ(3),[7 length(typ(3))-1]);
	     port_number=eval(port_number(2));
	     port_type=strsplit(typ(4),[6 length(typ(4))-1]);
	     [port_type,ok]=tran_type(port_type(2));
	     if ~ok then file('close',unit);return; end
	     if port_name=='Input' then
	     else
	       block_define.model.outtyp(port_number)=port_type;
	     end
        end
       end
     end
     ierr=execstr('block_define='+ga_name+'_cosf(block_define,par_name,par_exprs,par_val)','errcatch');
     if ierr then message('error. Please check the file '+ga_name+'_cosf');file('close',unit);return; end
     [x,y,typ]=getinputs(block_define);
     blknumvec=emptystr(size(x,'*'),1);
     blknumvec(:)=block_number_link;
     input_mat=[input_mat;[string(counter*ones(size(x,'*'),1)),..
                   blknumvec,..
                    string(x(:)),..
                     string(y(:)),..
                       string(typ(:))]]
     [x,y,typ]=getoutputs(block_define);
     blknumvec=emptystr(size(x,'*'),1);
     blknumvec(:)=block_number_link;
     output_mat=[output_mat;[string(counter*ones(size(x,'*'),1)),..
                   blknumvec,..
                    string(x(:)),..
                     string(y(:)),..
                       string(typ(:))]]
     disp(input_mat);
     disp(output_mat);
     scicosfile=[scicosfile;
                 lname+'.objs('+block_number+')=scicos_block(..';
		 '                   gui=""'+sci_name+'"",..';
		 '                   graphics=scicos_graphics(..']
     //scicos_graphics
     tt=getgraphics(block_define)
     scicosfile=[scicosfile;
		 '                           '+tt]
     //scicos_model
     tt=getmodel(block_define)
     scicosfile=[scicosfile;
		 '                  model=scicos_model(..';
		 '                            '+tt;
		 '                  doc=list())']
  elseif typ(1)=='<Connector' then
   disp('link')
   link_from=strsplit(typ(2),[10 length(typ(2))-1])
   link_from=link_from(2);
   port_from=strsplit(typ(3),[9 length(typ(3))-1]);
   port_from=port_from(2);
   link_to=strsplit(typ(4),[10 length(typ(4))-1])
   link_to=link_to(2);
   port_to=strsplit(typ(5),[9 length(typ(5))-3])
   port_to=port_to(2);
   frvc=[link_from port_from];
   if from_vec<>[] then
     ind=vectorfind(from_vec,frvc,'r');
   else
     ind=[]
   end
   if ind==[] then
     from_vec=[from_vec;frvc]
     counter=counter+1
     link_matrix=[link_matrix;..
	         [sci2exp(counter),link_from,port_from,link_to,port_to]];
   else
     // add the split to the diagram
     counter=counter+1
     block_number=sci2exp(counter)
     ierr=execstr('block_define=SPLIT_f('"define'")','errcatch')
     scicosfile=[scicosfile;
                 lname+'.objs('+block_number+')=scicos_block(..';
		 '                   gui=""SPLIT_f"",..';
		 '                   graphics=scicos_graphics(..']
     //scicos_graphics
     ndx=find(output_mat(:,2)==link_matrix(ind,2))
     ndx=ndx(eval(port_from))
     block_define.graphics.orig=[eval(output_mat(ndx,3))+5 eval(output_mat(ndx,4))]
     tt=getgraphics(block_define)
     scicosfile=[scicosfile;
		 '                           '+tt]
     //scicos_model
     tt=getmodel(block_define)
     scicosfile=[scicosfile;
		 '                  model=scicos_model(..';
		 '                            '+tt;
		 '                  doc=list())']
     link_matrix=[link_matrix;..
                 [sci2exp(counter+1) 'split'+block_number '1' link_matrix(ind,4) link_matrix(ind,5)];..
		 [sci2exp(counter+2) 'split'+block_number '2' link_to port_to]]
     link_matrix(ind,4:5)=['split'+block_number '1']
     [x,y,typ]=getinputs(block_define)
     input_mat=[input_mat;..
		[block_number,..
		  'split'+block_number,..
		    string(x),..
		     string(y),..
		      string(typ)]]
     [x,y,typ]=getoutputs(block_define)
     output_mat=[output_mat;..
		[[block_number;block_number],..
		  ['split'+block_number;'split'+block_number],..
		    string(x(1:2)'),..
		     string(y(1:2)'),..
		      string(typ(1:2)')]]
     counter=counter+2     
   end
  end
end
n=size(link_matrix(:,1));
n=n(1);
for i=1:n
  block_number=link_matrix(i,1);
  scicosfile=[scicosfile;
              lname+'.objs('+block_number+')=scicos_link(..';];
  link_val=scicos_link();
  k=find(output_mat(:,2)==link_matrix(i,2));
  k=k(eval(link_matrix(i,3)));
  link_from=[eval(output_mat(k,1)) eval(link_matrix(i,3)) 0];
  link_xx1=eval(output_mat(k,3));
  link_yy1=eval(output_mat(k,4));
  k=find(input_mat(:,2)==link_matrix(i,4));
  k=k(eval(link_matrix(i,5)));
  link_to=[eval(input_mat(k,1)) eval(link_matrix(i,5)) 1];
  link_val.from=link_from;
  link_val.to=link_to;
  link_xx2=eval(input_mat(k,3));
  link_yy2=eval(input_mat(k,4));
  link_val.xx=[link_xx1 link_xx2];
  link_val.yy=[link_yy1 link_yy2];
  tt=getlinktt(link_val);
  scicosfile=[scicosfile;
	      '                '+tt];
end
if rhs<2 then
  pause
  file('close',unit);
end
endfunction

function [num,ok]=tran_type(name)
ok=%t
if name=='double' then num=1;
elseif name=='ZZZ' then num=2;
elseif name=='int32' then num=3;
elseif name=='int16' then num=4;
elseif name=='int8' then num=5;
elseif name=='uint32' then num=6;
elseif name=='uint16' then num=7;
elseif name=='uint8' then num=8;
else message('An error occured while translating check the type'); ok=%f;return;
end
endfunction

function tt=getprops(props)
    tt=[];
    fields=getfield(1,props);
    for i=1:lstsize(props)-1
       field_nam=fields(i+1);
       if field_nam=='title' then field_nam='Title', end
          tt2=sci2exp(getfield(i+1,props),lmax);
          tt2(1)=field_nam+'='+tt2(1);
       if i<>lstsize(props)-1 then
          tt2($)=tt2($)+',..';
       else
	  tt2($)=tt2($)+'))';
       end
       tt=[tt;tt2];
     end
endfunction

function tt=getgraphics(block_define)
    tt=[];
    fields=getfield(1,block_define.graphics);
    for i=1:lstsize(block_define.graphics)-1
      field_nam=fields(i+1);
      tt2=sci2exp(getfield(i+1,block_define.graphics),lmax);
      tt2(1)=field_nam+'='+tt2(1);
      if i<>lstsize(block_define.graphics)-1 then
        tt2($)=tt2($)+',..';
      else
	tt2($)=tt2($)+'),..';
      end
      tt=[tt;tt2];
    end
endfunction

function tt=getmodel(block_define)
    tt=[];
    fields=getfield(1,block_define.model);
    for i=1:lstsize(block_define.model)-1
      field_nam=fields(i+1);
      if field_nam=='rpar'&(block_define.model.sim=='super'| block_define.model.sim=='csuper') then
        tt2='scs_m_'+string(count+1);
      else
        tt2=sci2exp(getfield(i+1,block_define.model),lmax);
      end
      tt2(1)=field_nam+'='+tt2(1);
      if i<>lstsize(block_define.model)-1 then
        tt2($)=tt2($)+',..';
      else
	tt2($)=tt2($)+'),..';
      end
      tt=[tt;tt2];
    end
endfunction

function tt=getlinktt(link_val)
  tt=[];
  txt=[];
  fields=getfield(1,link_val);
  for i=1:lstsize(link_val)-1
    field_nam=fields(i+1);
    tt2=sci2exp(getfield(i+1,link_val),lmax);
    tt2(1)=field_nam+'='+tt2(1);
    if i<>lstsize(link_val)-1 then
       tt2($)=tt2($)+',..';
    else
       tt2($)=tt2($)+')';
    end
    tt=[tt;tt2];
  end
endfunction