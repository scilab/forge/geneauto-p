function [o,needcompile,newparameters]=INPUTPORT(flag,o)
// Author : Fady Nassif,  Copyright INRIA, GPL Version 3, 29 June 2007  
  global labels values
  
  needcompile=%f,newparameters=%f
  if flag=="set" then
    values=list(o.model.ipar,[])
    labels=['Port';'InitialInput']
  end
endfunction
