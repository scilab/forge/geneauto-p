
function [txt,ok]=arraydatatype2xml(value,dims)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  if argn(2)==1 then dims=size(value),end
  select type(value)
  case 1 then
    id1=newxmlid()
    if prod(dims)==1 then
       txt=['<dataType type=""gaxml:object"">'
	    '    <TRealDouble '+id1+'/>'
	    '</dataType>']
    else
      id2=newxmlid()
      [xmldims,ok]=dimensions2xml(dims)
      if ~ok then txt=[];setxmlid(idstart),return,end
      txt=['<dataType type=""gaxml:object"">'
	   '    <TArray '+id1+'>'
	   '        <baseType type=""gaxml:object"">'
	   '            <TRealDouble '+id2+'/>'
	   '        </baseType>'
	   '       '+xmldims
	   '    </TArray>'
	   '</dataType>']
    end
  case 8 then
    ityp=inttype(value)
    nbits=modulo(ityp,10)*8
    id1=newxmlid()
    if prod(dims)==1 then
      if ityp>10 then
	txt=['<dataType type=""gaxml:object"">'
	     '    <TRealInteger '+id1+' nBits=""'+string(nbits)+'""/>'
	     '</dataType>']
      else
	txt=['<dataType type=""gaxml:object"">'
	     '    <TRealInteger '+id1+' nBits=""'+string(nbits)+'"" signed=""true""/>'
	     '</dataType>']
      end
    else
      id2=newxmlid()
      [xmldims,ok]=dimensions2xml(dims)
      if ~ok then txt=[];setxmlid(idstart),return,end
      if ityp>10 then
	txt=['<dataType type=""gaxml:object"">'
	     '    <TArray '+id1+'>'
	     '        <baseType type=""gaxml:object"">'
	     '            <TRealInteger '+id2+' nBits=""'+string(nbits)+'""/>'
	     '        </baseType>'
	     '       '+xmldims
	     '    </TArray>'
	     '</dataType>']
      else
	txt=['<dataType type=""gaxml:object"">'
	     '    <TArray '+id1+'>'
	     '        <baseType type=""gaxml:object"">'
	     '            <TRealInteger '+id2+' nBits=""'+string(nbits)+'"" signed=""true""/>'
	     '        </baseType>'
	     '       '+xmldims
	     '    </TArray>'
	     '</dataType>']
      end
    end
  else 
    txt=[];ok=%f
    errormessage('Variable with Scilab type '+string(type(value))+' are not yet handled')
    return
  end
endfunction
