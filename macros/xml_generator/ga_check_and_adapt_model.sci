function [ok,main_scs_m,freof]=ga_check_and_adapt_model(main_scs_m)
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007  

  global  lk  ksel 
  
  // - Check Scicos superbloc parameters
  [ok,params,param_types]=FindSBParams(main_scs_m,[])
  if ~ok then return;end
  if params<>[] then
    message(['The parameters'; 
	     '""'+params+'""',
	     ' must be defined in the context of the super block.'])
    ok=%f;
    return;
  end

  // - Check if the Scicos "if then else" blocs are connected to an
  // action superblock.
  [main_scs_m,ok]=ga_ifthel_test(main_scs_m);
  if ~ok then return,end

  // - Check if the superblock contains only identical sampleCLK
  [ok,freof]=ga_other_tests(main_scs_m)
  if ~ok then return,end
endfunction
