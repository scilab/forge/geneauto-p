// Convert expressions to Gene-Auto XML
  
// Authors : Fady Nassif, Serge Steer. INRIA. 2007-2009. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function [txt,ok]=ga_expr2xml(expr)
  idstart=getxmlid()

  deff('foo',expr)
  t=macr2tree(foo);t=t.statements;
  if size(t)<>6 then pause,end
  tree=t(2).expression;
  tree=simplify_tree(tree)

  if typeof(tree)=='variable' then
    [txt,ok]=ga_expressiontree2xml(tree)
    if ~ok then setxmlid(idstart); return,end
    txt=['<value type=""gaxml:object"">'
	 '    '+txt
	 '</value>']
  elseif typeof(tree)=='cste' then 
    [txt,ok]=value2xml(tree.value)
    if ~ok then setxmlid(idstart); return,end
  elseif %t& expr<>'%if_u1>0' then // %t added to force the use of the value
    txt=[];ok=%f
  else
    id=newxmlid()
    [txt,ok]=ga_expressiontree2xml(tree)
    if ~ok then setxmlid(idstart);return,end
   
    txt=['<ExpressionValue '+id+'>'
	 '    <value type=""gaxml:object"">'
	 '        '+txt
	 '    </value>'
	 '</ExpressionValue>']
  end
endfunction
