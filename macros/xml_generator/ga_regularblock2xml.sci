function [ok,xml,capt,actt,ports_association]=ga_regularblock2xml(scs_m,k,Bn,ports_association)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
//Generate GeneAuto XML representation of a regular block
  global  lk ksel scs_msav 
  global bllst corinv k1 //variables set in ga_scicos2xml
  t=[];capt=[];actt=[];xml=[]
  
  o=scs_m.objs(k)
  block_num=k;
  blocks_to_remove=['CLKSPLIT_f','SPLIT_f','IMPSPLIT_f','CLKSOM_f','CLKSOMV_f','NRMSOM_f','SampleCLK','TEXT_f','VirtualCLK0']
  if or(o.gui==blocks_to_remove) then ok=%t,xml=[],return,end
  sp_blocks=['GOTO','FROM','GotoTagVisibility']
  name=o.graphics.id;
  if length(name) == 0 then
      name=Bn+string(k);
  end
  values=list();labels=[];spt='-1';
  if o.model.sim(1)=='bidon' then k1=k1+1; ok=%t,return,end 

  [blktyp,labels,values,EXPRS,ok]=block_tran(o);
  if ~ok then
    mess=msprintf('%s block parameters failed to translate to GeneAuto', o.gui);
    disp(mess);
    scs_m=scs_msav
    hilite_path([ksel lk k],mess,%t);
    return; 
  end

  pal_typ=ga_find_pal_typ(blktyp);
  id_blk=newxmlid();
  
  np=size(labels,'*')
  if and(o.gui<>sp_blocks) then
    //???????
    //     for exprs_num=1:size(EXPRS,'*')
    //       to_change=find(matparm(:,1)==EXPRS(exprs_num))
    //       if to_change<>[] then
    // 	EXPRS(exprs_num)=matparm(max(to_change),2)
    //       end
    //     end
    k1=k1+1;     // a global counter that counts the standard blocks
    vv=bllst(k1);          // using the list bllst given by adjust_inout_xml to compute
    szin=[vv.in vv.in2];  // the input and output port size.
    szout=[vv.out vv.out2];
  end
  
  M=o.model
  
  [parameters,ok]=ga_parameters2xml(EXPRS,values,labels)
  if ~ok then
      mess=msprintf('%s block parameters failed to translate to GeneAuto XML', o.gui);
      disp(mess);
      
      hilite_path([ksel lk k],mess,%t);
      return
  end
  
  if or( o.gui==['DOLLAR','DOLLAR_f','DOLLAR_m','MathFunc','IFTHEL_f']) then
    if o.model.evtin<>[] then
      ln=scs_m.objs(o.graphics.pein);
      fromb=scs_m.objs(ln.from(1));
      while fromb.gui=="CLKSPLIT_f" do
	ln=scs_m.objs(fromb.graphics.pein);
	fromb=scs_m.objs(ln.from(1));
      end
      lktemp=lk;
      while fromb.gui=='CLKINV_f' do
	diagr=scs_m_sav;
	for i=1:size(lktemp,'*')-1
	  diagr=diagr.objs(lktemp(i)).model.rpar;
	end
	ln=diagr.objs(lktemp($)).graphics.pein;
	blknum=diagr.objs(ln).from(1);
	fromb=diagr.objs(blknum);
	while fromb.gui=="CLKSPLIT_f" do
	  ln=diagr.objs(fromb.graphics.pein);
	  fromb=diagr.objs(ln.from(1));
	end
	lktemp($)=[];
      end
      if or(fromb.gui==['SampleCLK']) then
	val=fromb.model.rpar;
	exprs=fromb.graphics.exprs(1);
	spt=formatrealnumber(val(1));
      end
    end
  end
 
  if and(o.gui<>blocks_to_remove)& and(o.gui<>sp_blocks) then
    [xmlin,pain,ok]=ga_InDataPorts2xml(vv,block_num)
    if ~ok then
        mess=msprintf('%s block inputs failed to translate to GeneAuto XML', o.gui);
        disp(mess);
        return
    end
    [xmlout,paout,ok]=ga_OutDataPorts2xml(vv,block_num)
     if ~ok then
        mess=msprintf('%s block outputs failed to translate to GeneAuto XML', o.gui);
        disp(mess);
        return
     end
    
    if (o.gui=='IFTHEL_f') then
      if o.graphics.peout(2)<>0 then 
	nport=2,
      else
	nport=1
      end
      [xmlevt,paevt,ok]=ga_OutControlPorts2xml(nport,block_num)
      if ~ok then
        mess=mprintf('%s block controls failed to translate to GeneAuto XML', o.gui);
        disp(mess);
      end
    else
      xmlevt=[],paevt=[]
    end
  else
    xmlin=[],pain=[],xmlout=[],paout=[];xmlevt=[],paevt=[],
  end
  
  try
    externalID="externalID=""" + o.doc(1) + """";
    originalFullName="originalFullName=""" + o.doc(1) + """";
  catch
    externalID="";
    originalFullName="";
  end
  
  xml=['<'+pal_typ+'Block directFeedThrough=""true"" '+id_blk+' '+externalID+' '+originalFullName+' isVirtual=""false"" name=""'+name+'"" sampletime =""'+spt+'"" type=""'+blktyp+'"">'
       '     '+xmlin
       '     '+xmlout
       '     '+xmlevt
       '     '+parameters
       '     '+ga_DiagramInfo(ga_block_pos(o))
       '</'+pal_typ+'Block>' ];

  ports_association=[ports_association;
		     pain
		     paout
		     paevt]
  


  if o.gui=='INPUTPORT' then
    capt=[string(vv.ipar) string(vv.out) string(vv.out2) string(vv.outtyp) name string(1)];
  end
  if o.gui=='OUTPUTPORT' then
    actt=[string(vv.ipar) string(vv.in) string(vv.in2) string(vv.intyp) name string(1)];
  end
endfunction


