function rect=GetBounds(scs_m)
//Return bounding box of a scicos diagram

// Author : Fady Nassif,  Copyright INRIA, GPL Version 3, 29 June 2007  
  rect=[];
  
  if rect==[] then rect=[0 0 600 400],end
  wsiz=[rect(3)-rect(1),rect(4)-rect(2)];

  wdd=min(1200,max(rect(3)-rect(1),300))
  hdd=min(1000,max(rect(4)-rect(2),200))
  rect=[rect(1) rect(2) rect(1)+wdd rect(2)+hdd]
endfunction

