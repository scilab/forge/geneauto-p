
function [txt,ok]=cc2xml(operands)
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  txt=['<ListExpression '+newxmlid()+'>'
       '    <expressions type=""gaxml:collection"">'
       '        <ListExpression '+newxmlid()+'>'	 
       '            <expressions type=""gaxml:collection"">']
  //loop on operands
  for op=operands
    [xmlop,ok]=ga_expressiontree2xml(op)
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=[txt;
	 '                '+xmlop]
  end
  txt=[txt;
       '            </expressions>'
       '        </ListExpression>'
       '    </expressions>'
       '</ListExpression>' ]
endfunction
