
function [txt,ok]=String2xml(value)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007

  txt='<StringValue '+newxmlid()+' value=""'+ga_xml_subst(value)+'""/>'
  ok=%t
endfunction
