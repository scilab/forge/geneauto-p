function [ok,xmlblks,xmlllks,xmlcontext,capt,actt]=ga_blocks2xml(scs_m,Bn,matparm)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3,
// 29 June 2007
    
// Generate XML GeneAuto representation of a Scicos block 
// Arguments:
// scs_m   : scicos data structure of a superblock.
// Bn      : string, the base for block naming
// matparm :  
//
// ok      : boolean true if no error occured
// xmlblks     : column vector of strings (the GeneAuto XML representation of the block)
// xmlllks     : column vector of strings (the GeneAuto XML representation of the internal links)
// xmlcontext   : column vector of strings (the GeneAuto XML representation of the context)
// capt    . input ports properties  
// actt    . output ports properties  
// Authors : Fady Nassif, Serge Steer, Copyright INRIA  
  xmlblks=[];xmlcontext=[]; capt=[];actt=[];xmlllks=[];ports_association=[];link_to_treat=[];ok=%t
  port_blocks=['IN_f','INIMPL_f','OUT_f','OUTIMPL_f','CLKIN_f','CLKINV_f','CLKOUT_f','CLKOUTV_f']
 
  //Walk subtree in the Scicos objects hierarchy
  for k=1:size(scs_m.objs)
    o=scs_m.objs(k);
    otyp=typeof(o);
    if otyp=='Block' then //object is a block
      if or(o.gui==port_blocks) then //Port block
        [ok,bb,ports_association,cap,act]=ga_portblock2xml(scs_m,k,Bn,ports_association)
	if ~ok then return,end
	xmlblks=[xmlblks;bb]
	capt=[capt;cap]
	actt=[actt;act]
      elseif or(o.model.sim(1)==['super','csuper']) then // Super Block
	[ok,xmlblkssup,xmlcontextsup,ports_association]=ga_superblock2xml(scs_m,k,Bn,matparm,ports_association,%f)
	if ~ok return,end
	xmlblks=[xmlblks;
	     '  '+xmlblkssup];
	xmlcontext=[xmlcontext;
	       xmlcontextsup];
      elseif o.model.sim(1)=='asuper' then // Atomic Super Block
	[ok,xmlblkssup,xmlcontextsup,ports_association]=ga_superblock2xml(scs_m,k,Bn,matparm,ports_association,%t)
	if ~ok return,end
	xmlblks=[xmlblks;
	     '  '+xmlblkssup];
	xmlcontext=[xmlcontext;
	       xmlcontextsup];
      else //standard block
	[ok,xmlblksblock,cap,act,ports_association]=ga_regularblock2xml(scs_m,k,Bn,ports_association)
	if ~ok then return,end
	xmlblks=[xmlblks;xmlblksblock]
	capt=[capt;cap]
	actt=[actt;act]
      end
    elseif or(otyp==['Deleted' 'Text']) then
      //these objects are ignored
    else //link
      link_to_treat=[link_to_treat,k]
    end
  end //end of loop on objects
  for i=link_to_treat
      [lnks]=ga_link2xml(scs_m,i,ports_association)
      xmlllks=[xmlllks;lnks];
  end
 endfunction
 
