function [vv,port]=inoutport(scs_m,block,port,corinv,lkv,bllst,flg)
// Author : Fady Nassif,  Copyright INRIA, GPL Version 3, 29 June 2007  
 
numb=findinlist(corinv,[lkv block])
if numb<>list() then
  vv=bllst(numb);
  return;
elseif (scs_m.objs(block).gui=='IN_f') then 
  o1=scs_m.objs(block);
  scs_mt=scs_m_sav;
  while ((o1.model.sim(1)=='lsplit')| (o1.gui=='IN_f')) do
    while (o1.model.sim(1)=='lsplit') do
      if size(lkv,'*')==1 then scs_mt=scs_m_sav;end
      for i=1:size(lkv,'*')-1
	scs_mt=scs_mt.objs(lkv(i)).model.rpar;
      end      
      lnk=scs_mt.objs(o1.graphics.pin);
      block=lnk.from(1);
      port=lnk.from(2);
      o1=scs_mt.objs(block);
    end
    while (o1.gui=='IN_f') do
      if size(lkv,'*')==1 then scs_mt=scs_m_sav;end
      for i=1:size(lkv,'*')-1
	scs_mt=scs_mt.objs(lkv(i)).model.rpar;
      end
      numlnk=scs_mt.objs(lkv($)).graphics.pin;
      block=scs_mt.objs(numlnk).from(1);
      port=scs_mt.objs(numlnk).from(2);
      o1=scs_mt.objs(block);
      lkv($)=[];
    end
  end
  numb=findinlist(corinv,[lkv block])
  vv=bllst(numb);
elseif (scs_m.objs(block).gui=='OUT_f') then 
  o1=scs_m.objs(block);
  scs_mt=scs_m_sav;
  while ((o1.model.sim(1)=='lsplit')| (o1.gui=='OUT_f')) do
    while (o1.model.sim(1)=='lsplit') do
      if size(lkv,'*')==1 then scs_mt=scs_m_sav;end
      for i=1:size(lkv,'*')-1
	scs_mt=scs_mt.objs(lkv(i)).model.rpar;
      end
      for i=1:size(o1.graphics.pout,'*')
	lnk=scs_mt.objs(o1.graphics.pout(i));
	block=lnk.from(1);
	port=lnk.from(2);
	o1=scs_mt.objs(block);	
	if scs_mt.objs(block).model.sim(1)<>'lsplit' then
	  break;	  
	end
      end      
    end
    while (o1.gui=='OUT_f') do
      if size(lkv,'*')==1 then scs_mt=scs_m_sav;end
      for i=1:size(lkv,'*')-1
	scs_mt=scs_mt.objs(lkv(i)).model.rpar;
      end
      numlnk=scs_mt.objs(lkv($)).graphics.pout;
      block=scs_mt.objs(numlnk).to(1);
      port=scs_mt.objs(numlnk).to(2);
      o1=scs_mt.objs(block);
      lkv($)=[];
    end
  end
  numb=findinlist(corinv,[lkv block])
  vv=bllst(numb);
else
  lkv=[lkv block]
  scs_mt=scs_m.objs(block).model.rpar
  for k=1:size(scs_mt.objs)
    o=scs_mt.objs(k);
    x=getfield(1,o);
    if (x(1)=='Block') then 
      if flg==1 then
	if or(o.gui=='IN_f') then 
	  if o.model.ipar==port then
	    lnk=scs_mt.objs(o.graphics.pout);
	    block=lnk.to(1);
	    port=lnk.to(2);
	    o1=scs_mt.objs(block);
	    while (o1.model.sim(1)=='lsplit') do
	      lnk=scs_mt.objs(o1.graphics.pout(port));
	      block=lnk.to(1);
	      port=lnk.to(2);
	      o1=scs_mt.objs(block);
	    end
	    [vv,port]=inoutport(scs_mt,block,port,corinv,lkv,bllst,1);
	    break;
	  end
	end
      else
	if or(o.gui=='OUT_f') then
	  if o.model.ipar==port then
	    lnk=scs_mt.objs(o.graphics.pin);
	    block=lnk.from(1);
	    port=lnk.from(2);
	    o1=scs_mt.objs(block);
	    while (o1.model.sim(1)=='lsplit') do
	      lnk=scs_mt.objs(o1.graphics.pin(port));
	      block=lnk.from(1);
	      port=lnk.from(2);
	      o1=scs_mt.objs(block);
	    end
	    [vv,port]=inoutport(scs_mt,block,port,corinv,lkv,bllst,2);
	    break;
	  end
	end
      end
    end
  end
end
endfunction
     
