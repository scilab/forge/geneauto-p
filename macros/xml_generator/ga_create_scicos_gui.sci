function [ok,name]=ga_create_scicos_gui(tit,capt,actt,freof,dest);
//Generates the Scilab interfacing function for GeneAuto generated block
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007  
  if freof<>[] then evtin='                      evtin=1,..',else evtin=[],end
  name=tit+'_gagui'
  Code=['function [x,y,typ]='+name+'(job,arg1,arg2)';
        '// Copyright INRIA';
        ' x=[];y=[];typ=[];';
        ' select job';
        ' case ''plot'' then';
        '   standard_draw(arg1)';
        ' case ''getinputs'' then';
        '   [x,y,typ]=standard_inputs(arg1)';
        ' case ''getoutputs'' then';
        '   [x,y,typ]=standard_outputs(arg1)';
        ' case ''getorigin'' then';
        '   [x,y]=standard_origin(arg1)';
        ' case ''set'' then';
        '   x=arg1;';
        ' case ''define'' then'
        '   model=scicos_model(sim=list('''+tit+'_blk'',4),..'
        '                      in='+sci2exp(evstr(capt(:,2)),0)+',..'
        '                      in2='+sci2exp(evstr(capt(:,3)),0)+',..'
        '                      intyp='+sci2exp(evstr(capt(:,4))',0)+',..'
        '                      out='+sci2exp(evstr(actt(:,2)),0)+',..'
        '                      out2='+sci2exp(evstr(actt(:,3)),0)+',..'
        '                      outtyp='+sci2exp(evstr(actt(:,4))',0)+',..'
	evtin
	'                      blocktype=''d'')'
        '   gr_i=''xstringb(orig(1),orig(2),'''''+tit+''''',sz(1),sz(2),''''fill'''')''';
        '   x=standard_define([2 2],model,[],gr_i)';
	'   x.graphics.id=''GeneAuto''';
        ' end'
        'endfunction'];
  ok=execstr('mputl(Code,dest+name+''.sci'')','errcatch')==0
  if ~ok then message(lasterror()),end
endfunction
