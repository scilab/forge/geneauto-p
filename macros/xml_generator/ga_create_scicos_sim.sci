function ok=ga_create_scicos_sim(src_path,tit,cap,act)
// build files used to make the connection between the GeneAuto generated
// code and Scicos
// src_path: path where the C code of the Scicos interface will be stored
// tit : block name
// cap : array with 5 column. Each row contains an input port property
//       [Number  row_size col_size type number_of_block]
// act :  array with 5 column. Each row contains an output port property
//       [Number  row_size col_size type number_of_block]

// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007  
  ok=%t;
//  tit1=convstr(part(tit,1))+convstr(part(tit,1))+part(tit,2:length(tit))
  //Create simulation function C code
//  capt=[capt ones(size(capt,1),1)];
//  act=[act -ones(size(act,1),1)];
//  all_ports=[capt;act];
//  [v,ind]=gsort(all_ports(:,5),'g','i');
//  all_ports=all_ports(ind(:),:);
//  all_ports=[all_ports countobj+2*[1:size(all_ports,1)]']
//  ind=find(all_ports(:,6)==1);
//  capt=all_ports(ind,:);
//  ind=find(all_ports(:,6)==-1);
//  act=all_ports(ind,:)
//  [v,ind]=gsort(capt(:,1),'g','i')
//  capt=capt(ind(:),:);
//  [v,ind]=gsort(act(:,1),'g','i')
//  act=act(ind(:),:);

  txt=[
      '#include <scicos_block4.h>'
      '#define GetBoolOutPortPtrs(blk,x) Getint32OutPortPtrs(blk,x)'
      '#define GetBoolInPortPtrs(blk,x) Getint32InPortPtrs(blk,x)'
      '#include ""'+tit+'.h""'
      'void '+tit+'_blk'+'(scicos_block *block,int flag)'
      '{'
      '  IO_'+tit+'_compute  *C;'
      '  if (flag ==4){/*initialization */'
      '    if ((*block->work=scicos_malloc(sizeof(IO_'+tit+'_compute)))==NULL) {'
      '      set_block_error(-16);'
      '      return;'
      '    }'
      '    C=*block->work;'
      '    '+tit+'_init();'
      '  }'
      '  else if (flag == 1){/*compute output*/'
      '    C=*block->work;'
      '   '+GenInput(capt);
      '   '+tit+'_compute(C);'
      '   '+GenOutput(act);
      '  }'
      '  else if (flag == 5){/*finish output*/'
      '    scicos_free(*block->work);'
      '  }'
      '}']
  // Write it
  ok=execstr('mputl(txt,src_path+tit+''_blk.c'')','errcatch')==0
  if ~ok then message(lasterror()),return,end
 
  //next line because scicos_block4.h forgotten in scilab-4.1.2 binary
  //version
//  p=ga_path()
//  [status,msg]=copyfile(p+'src/scicos_block4.h',src_path+'scicos_block4.h')
//  ok=status==0
  //if ~ok then message(msg),end
endfunction

function I= GenInput(capt)
//Generate C code which copy the inputs fronm the Scicos block structure
//to GeneAuto block structure
  ttypes=['Real','ZZZZZ','int32','int16','int8','uint32','uint16','uint8','Bool']
  I=[];//C code to set the inputs
  nm=capt(:,5);
  capt=evstr(capt(:,1:4))
  for i=1:size(capt,1)
    if and(capt(i,2:3)==1) then //scalars
      I=[I;
	 'C->'+nm(i)+'=*Get'+ttypes(capt(i,4))+'InPortPtrs(block,'+string(i)+');'];
    elseif or(capt(i,2:3)==1) then  //row and column vectors ??? (see GenOutput)
      for k=0:prod(capt(i,2:3))-1
	I=[I;
	   'C->'+nm(i)+'['+string(k)+']=*(Get'+ttypes(capt(i,4))+'InPortPtrs(block,'+string(i)+')+'+string(k)+');'];
      end
    else matrices
      j=0;
      for k=0:capt(i,3)-1
	for l=0:capt(i,2)-1
	  I=[I;
	     'C->'+nm(i)+'['+string(l)+']['+string(k)+']=*(Get'+ttypes(capt(i,4))+'InPortPtrs(block,'+string(i)+')+'+string(j)+');'];
	  j=j+1;
	end
      end
    end
  end
endfunction

function O= GenOutput(actt)
//Generate C code which copy the outputs fronm the GeneAuto block structure
//to Scicos block structure
  ttypes=['Real','ZZZZZ','int32','int16','int8','uint32','uint16','uint8','Bool']
  O=[];//C code to set the outputs
  nm=actt(:,5);
  actt=evstr(actt(:,1:4))

  for i=1:size(actt,1)
    if and(actt(i,2:3)==1) then //scalars
      O=[O;
	 '*Get'+ttypes(actt(i,4))+'OutPortPtrs(block,'+string(i)+')=C->'+ ...
	 nm(i)+';'];

     elseif or(actt(i,3)==1) then //   GASM vectors are represented by column vectors, 
       for k=0:prod(actt(i,2:3))-1
 	O=[O;
 	   '*(Get'+ttypes(actt(i,4))+'OutPortPtrs(block,'+string(i)+')+'+string(k)+')=C->'+nm(i)+'['+string(k)+'];'];
       end
    else //row vector ans matrices
      j=0;
      for k=0:actt(i,3)-1
	for l=0:actt(i,2)-1
	  O=[O;
	     '*(Get'+ttypes(actt(i,4))+'OutPortPtrs(block,'+string(i)+')+'+string(j)+')=C->'+nm(i)+'['+string(l)+']['+string(k)+'];'];
	  j=j+1;
	end
      end
    end
  end
endfunction
  
