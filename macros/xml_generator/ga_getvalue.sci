function varargout=ga_getvalue(a,b,c,d)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  global par_types
  par_types=c
  //simulate a "Cancel" to exit from block set action
  ok=%f;
  varargout=list(ok);
  for k=2:argn(1), varargout($+1)=[],end
endfunction
