function [x,y,typ]=ACTION(job,arg1)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
  x=[];y=[],typ=[]
  select job
  case 'set' then
    x=arg1,y=%f,typ=%f
  case 'define' then
    newmodel=scicos_model();
    newmodel.sim='action'
    gr_i=['xstringb(orig(1),orig(2),ACTION,sz(1),sz(2),''fill'');']
    x=standard_define([2 2],newmodel,'',gr_i)
  end
endfunction
