function [txt,ok]=ga_parameters2xml(exprs,values,labels)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  ok=%t;txt=[];
  if type(values)<>15 then 
    mprintf('Current block values are not given in a list')
    pause,
  end
  np=size(values)
  if np==0 then
    return
//  elseif np==1 then
//    [txt,ok]=ga_parameter2xml(exprs(1),values(1),labels)
  else
    txt='<parameters type=""gaxml:collection"">';
    for k=1:np
      [xml,ok]=ga_parameter2xml(exprs(k),values(k),labels(k))
      if ~ok then txt=[];return,end
      txt=[txt;
	   '    '+xml]
    end
    txt=[txt;
	 '</parameters> ']
  end
endfunction

function [Parameter,ok]=ga_parameter2xml(expr,val,label)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  global Scicos2GASM_error
  Scicos2GASM_error=[];
  idstart=getxmlid()
  id1=newxmlid()
  ok=%t
  if expr=='' then //no available expression, use the value 
    [txt,ok]=value2xml(val)
    if ~ok then message(Scicos2GASM_error);Parameter=[];setxmlid(idstart),return,end
    Parameter=['<BlockParameter '+id1+' name=""'+label+'"">'
	       '    <value type=""gaxml:object"">'
	       '        '+txt
	       '    </value>'
	       '</BlockParameter>'];
    return
  end
 
  //try to convert the Scilab expression into xml
  [txt,ok]=ga_expr2xml(expr)
  if ~ok then
    mprintf('%s\n',['The expression ""'+expr+'"" used to define a parameter  '+label
		    '  cannot be translated:'
		    '  '+Scicos2GASM_error
		    '  The current value is used.'] )
    Scicos2GASM_error=[];
    setxmlid(idstart)
    id1=newxmlid()
    [txt,ok]=value2xml(val)
    if ~ok then message(Scicos2GASM_error);Parameter=[];setxmlid(idstart),return,end
    Parameter=['<BlockParameter '+id1+' name=""'+label+'"">'
	       '    <value type=""gaxml:object"">'
	       '        '+txt
	       '    </value>'
	       '</BlockParameter>'];
  else
    Parameter=['<BlockParameter '+id1+' name=""'+label+'"">'
	       '    <value type=""gaxml:object"">'
	       '        '+txt
	       '    </value>'
	       '</BlockParameter>']
  end
endfunction
