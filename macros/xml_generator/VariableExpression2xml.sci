function  [txt,ok]=VariableExpression2xml(name)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  if name=='%if_u1' then //special case for IfExpression
      txt='<VariableExpression '+newxmlid()+' name=""u1""/>'
      ok=%t
      return
  end
  k=find(name==matparm(:,1))
  if k==[] then 
     errormessage('Variable named ""'+name+'"" is undefined')
     txt=[]
     ok=%f
     return
  end
  k=k($)
  if name=='%pi' then name='pi',end
  if name=='%eps' then name='eps',end
  //???? Depuis mdl le tag VariableExpression comprend le sous tag <dataType
  //type="gaxml:object"> est-il necessaire?
  //si oui il faut retrouver le type et les dims  dans %scicos_context
  txt=['<VariableExpression '+newxmlid()+' name=""'+matparm(k,2)+'"">'
       '    <variable type=""gaxml:pointer"">'+matparm(k,3)+'</variable>'
       '</VariableExpression>'   ]
  ok=%t
endfunction
