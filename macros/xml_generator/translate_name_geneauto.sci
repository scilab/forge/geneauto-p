function [name,labels,values]=translate_name_geneauto(name1,name2,label,value)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007  
//  select name1
labels=[];
values=list();
if or(name1==['PROD_f','PRODUCT','MATMUL','INTMUL']) then 
  name='Product'
  labels='Inputs'
  values=''
  if name1=='PRODUCT' then
    if size (value(1),'*')==1 then
      for i=1:value(1)
	values=strcat([values,'*'])
      end
    else
      for i=1:size(value(1),'*')
	if value(1)(i)==1 then
	  values=strcat([values,'*'])
	elseif value(1)(i)==-1 then
	  values=strcat([values,'/'])
	end
      end
    end
  else
    values='**';
  end
elseif or(name1==['GAIN_f','GAINBLK','GAINBLK_f']) then
  name='Gain'
  labels=['Gain';'Multiplication']
  if size(value(1),'*')==1 then
    values(2)='Element-wise(K.*u)'
  else 
    values(2)='Matrix(K*u)'
  end
  values(1)=value(1)
elseif or(name1==['SOM_f','SUM_f']) then
  name='Sum'
  labels='Inputs'
  values='+++'
elseif or(name1==['SUMMATION']) then
  name ='Sum'
  labels='Inputs'
  values='';
  if size (value(2),'*')==1 then
    for i=1:value(2)
      values=strcat([values,'+'])
    end
  else
    for i=1:size(value(2),'*')
      if value(2)(i)==1 then
	values=strcat([values,'+'])
      elseif value(2)(i)==-1 then
	values=strcat([values,'-'])
      end
    end
  end
elseif or(name1=='INPUTPORT') then
  name ='Inport'
  labels=label
  values=value
elseif or(name1=='OUTPUTPORT') then
  name ='Outport'
  labels=label
  values=value
elseif or(name1==['RELATIONALOP','RELATIONAL_OP']) then
  name= 'RelationalOperator'
  labels='Operator'
  OPER=['==','~=','<','<=','>','>=']
  values=OPER((value($-1))+1)
elseif or(name1==['DOLLAR','DOLLAR_f','DOLLAR_m']) then
  name= 'UnitDelay'
  labels='X0'
  values= list(value(1))
elseif or(name1==['SWITCH2','SWITCH2_m']) then
  name= 'Switch'
  labels= ['Criteria','Threshold'];
  values(2)=value($-1);
  OPER=['u2 >= Threshold','u2 > Threshold','u2 ~= 0'];
  values(1)=OPER(value($-1)+1)
elseif or(name1==['MUX','MUX_f']) then
  name= 'Mux'
  values=list();
  labels=[];
elseif or(name1==['DEMUX','DEMUX_f']) then
  name= 'Demux'
  values=list();
  labels=[];
elseif or(name1==['CONST','CONST_m','CONST_f']) then
  name= 'Constant'
  labels='Value'
  values= value
elseif (name1=='LOGICAL_OP') then
  labels=['Operator','Inputs']
  OPER=['AND','OR','NAND','NOR','XOR','NOT']
  values(1)=OPER(value(2)+1)
  values(2)=value(1);
  if value(4)==0 then
    name='Logic'
  else 
    name='BitwiseOperator'
  end 
elseif or(name1==['CFSCOPE','CMSCOPE','CSCOPEXY3D','CSCOPEXY','CSCOPE']) then
  name='Scope'
  labels=[]
  values=list()
elseif or(name1==['AFFICH_f','AFFICH_m']) then
  name='Display'
  labels=[]
  values=list()
elseif name1=='Div' then
  name=name1;
  labels='Inputs';
  values=list(2);
elseif or(name1==['Neg','DelayRE','Terminator','SampleAndHold']) then
  name=name1;labels=[];values=list()
elseif name1=='Ground_g' then name='Ground';labels=[];values=list(); 
elseif name1=='F_g' then 
  name='F'
  labels='SK.PARH1'
  values=value
elseif name1=='G_g' then
  name='G'
  labels='SK.PARH1'
  values=value
elseif name1=='ABS_VALUE' then name='Abs';labels=[];values=list();	  
elseif name1=='BascR' then   
  name=name1;
  if label==[] then labels=[];values=list();
  else labels=['SK.PARH1';'SK.PARH2'];values=value; 
  end
elseif name1=='BascS' then   
  name=name1;
  if label==[] then labels=[];values=list();
  else labels=['SK.PARH1';'SK.PARH2'];values=value; 
  end
elseif name1=='LIM' then	   
  name=name1
  if label==[] then labels=[];values=list();
  else labels=['SK.PARH1';'SK.PARH2'];values=value; 
  end
elseif name1=='MAXMIN' then	   
  name='MinMax';
  labels=['Function';'Inputs'];
  fn=['min';'max']
  values=list(fn(value(1)),value(2)); 
elseif name1=='SIGNUM' then name='Signum';labels=[];values=list(); 
elseif name1=='MathFunc' then 
  name='Math'
  labels='Operator'
  if name2=='modulos' then values='Modulo'; 
  else values=name2;
  end
elseif name1=='M_SWITCH' then 	
  name='MultiPortSwitch';
  labels='Inputs';
  values=value(1);
elseif name1=='GOTO' then
  name='Goto' 
  labels=['GotoTag';'TagVisibility']
  op=['local','scoped','global']
  values=list(value(1),op(value(2)))
elseif name1=='FROM' then
  name='From';
  labels='GotoTag'
  values=value
elseif name1=='GotoTagVisibility' then
  name=name1
  labels='GotoTag'
  values=value
elseif name1=='CONVERT' then name='DataTypeConversion';labels=[];values=list();
elseif name1=='FROMWS_c' then 
  name='FromWorkspace'
  labels='VariableName'
  values=value(1);
elseif name1=='TOWS_c' then
  name='ToWorkspace'
  labels='VariableName'
  values=value(1)
elseif name1=='TrigFun' then
  name='Trigonometry';
  labels='Operator';
  values=value(1);
else 
  name=name2
  values=value
  labels=label
  // case or(['Product','Div','Neg','Gain','Sum','RelationalOperator','UnitDelay','DelayRE',..
  //        'Logic','BitwiseOperator','Bitwise','Pulse1',']) then
//         name= ''
// case or(['']) then
//       name= ''
end
endfunction  


