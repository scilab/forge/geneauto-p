function [ok,xml,ports_association,capt,actt]=ga_portblock2xml(scs_m,k,Bn,ports_association)
//disp('ga_portblock2xml')
// Generate XML GeneAuto representation of a port block
// Arguments:
// scs_m : scicos data structure of a superblock.
// k     : index of the port block in the superblock  
// Bn    : string, the base for block naming
  
//
// xml   : column vector of strings (the GeneAuto XML representation of the superblock)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  capt=[];actt=[];xml=[],ok=%t
  global bllst corinv lk//variables set in ga_scicos2xml 
  idstart=newxmlid()
  blocks_type=['double','double','int32','int16','int8','uint32','uint16','uint8','boolean']
  o=scs_m.objs(k)
  blk_num=k;

  name=o.graphics.id;
  if length(name) == 0 then
      name=Bn+string(k);
  end

  if or(o.gui=='IN_f') then //Input superblock port
    lnk=scs_m.objs(o.graphics.pout);
    //find the real link origin through splits
    to_block=lnk.to(1);
    to_port=lnk.to(2);
    o1=scs_m.objs(to_block);
    while (o1.model.sim(1)=='lsplit') do
      lnk=scs_m.objs(o1.graphics.pout(to_port));
      to_block=lnk.to(1);
      to_port=lnk.to(2);
      o1=scs_m.objs(to_block);
    end
    [vv,to_port]=inoutport(scs_m,to_block,to_port,corinv,lk,bllst,1);
    Outtyp=blocks_type(vv.intyp(to_port))
    szin=[vv.in(to_port) vv.in2(to_port)]
    capt=[string(o.model.ipar) string(szin(1)) string(szin(2)) string(vv.intyp(to_port)) name string(0)];
    M=o.model

    o.model.out=szin(1);
    o.model.out2=szin(2);
    o.model.outtyp=vv.intyp(to_port);
    
    [xmlout,paout,ok]=ga_OutDataPorts2xml(o.model,blk_num)
    if ~ok then return,end
    labels= [ 'DataType','PortDimensions','Port']
    vals=list( 'auto',       int8(-1), string(M.ipar))
    exprs=  [    '',          ''               ''  ]

    
//    labels=["DataType","Port","SignalType","InitialValue","PortDimensions"]
//    vals=list(Outtyp,M.ipar,'auto',0,-1)
//    //????verifier que le rendu de PortDimension est ok
//    exprs=['','','','',''];
    [parameters,ok]=ga_parameters2xml(exprs,vals,labels)
    if ~ok then return,end
    xml=['<SourceBlock directFeedThrough=""true"" '+idstart+' isVirtual=""false"" name=""'+name+'"" sampleTime=""-1"" type=""Inport"">'
         '    '+xmlout
	 '    '+parameters
	 '    '+ga_DiagramInfo(ga_block_pos(o))
         '</SourceBlock>']
    ports_association=[ports_association;paout];    

  elseif or(o.gui=='OUT_f') then//Output superblock port
    lnk=scs_m.objs(o.graphics.pin);
    //find the real link origin through splits
    from_block=lnk.from(1);
    from_port=lnk.from(2);
    o1=scs_m.objs(from_block);
    while (o1.model.sim(1)=='lsplit') do
      lnk=scs_m.objs(o1.graphics.pin);
      from_block=lnk.from(1);
      from_port=lnk.from(2);
      o1=scs_m.objs(from_block);
    end
    [vv,from_port]=inoutport(scs_m,from_block,from_port,corinv,lk,bllst,2);
    Intyp=blocks_type(vv.outtyp(from_port))
    szout=[vv.out(from_port) vv.out2(from_port)]
    actt=[string(o.model.ipar) string(szout(1)) string(szout(2)) string(vv.outtyp(from_port)) name string(0)];
    M=o.model

    initv=sci2exp(zeros(szout(1),szout(2)),0)

    o.model.in=szout(1);
    o.model.in2=szout(2);
    o.model.intyp=vv.outtyp(from_port);

    [xmlin,pain,ok]=ga_InDataPorts2xml(o.model,blk_num)
    if ~ok then return,end
    labels=["DataType" "InitialOutput" "PortDimensions" "OutputWhenDisabled" "Port"]
    vals=list('auto',[],int8(-1),"held",string(M.ipar) )
    exprs=['' '','' '' ''];
    
    [parameters,ok]=ga_parameters2xml(exprs,vals,labels)
    if ~ok then return,end
    xml=['<SinkBlock directFeedThrough=""true"" '+idstart+' isVirtual=""false"" name=""'+name+'"" sampleTime=""-1"" type=""Outport"">'
         '    '+xmlin
	 '    '+parameters
	 '     '+ga_DiagramInfo(ga_block_pos(o))
         '</SinkBlock>']
    ports_association=[ports_association;pain];    
  end
endfunction
