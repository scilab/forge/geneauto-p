function [txt,ports_association,ok]=ga_OutDataPorts2xml(model,blk_num)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  txt=[];ports_association=[];ok=%t;
  nports=size(model.out,'*')
  
  if nports>0 then
    txt='<outDataPorts type=""gaxml:collection"">'

    try
        labels= " name=""" + o.graphics.out_label;
        labels= labels + """ ";
    catch
        // create empty (white space) labels
        labels=emptystr(nports, 1);
    end

    for i=1:nports
      port_id=getxmlid()
//       txt=[txt;
// 	   '    <OutDataPort '+newxmlid()+' portNumber=""'+string(i)+'"">'
// 	   '        '+ga_assign_type_dimension(model,i,'out')
// 	   '     </OutDataPort>']
       txt=[txt;
	    '    <OutDataPort '+newxmlid()+' portNumber=""'+string(i)+'""'+labels(i)+'/>']
      ports_association=[ports_association;
			 blk_num i port_id -1 1]
    end
    txt=[txt;  
	    '</outDataPorts>']
  end
endfunction
