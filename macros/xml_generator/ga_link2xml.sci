function xml=ga_link2xml(scs_m,k,ports_association)
 // Generate XML GeneAuto representation of a Scicos link 
// Arguments:
// scs_m   : scicos data structure of a superblock.
// k index of the link in the superblock
//
// xml     : column vector of strings (the GeneAuto XML representation of the block)
  
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  xml=[];
  o=scs_m.objs(k);
  from=o.from;
  to=o.to;
  // this part to eliminate the split block and connect directly the 
  // source block to the destination block 
  from_block=o.from(1)
  from_port=o.from(2)
  while (scs_m.objs(from_block).gui== 'SPLIT_f') do
    lnk=scs_m.objs(scs_m.objs(from_block).graphics.pin)
    from_block=lnk.from(1);
    from_port=lnk.from(2);
  end
  while (scs_m.objs(from_block).gui== 'CLKSPLIT_f') do
    lnk=scs_m.objs(scs_m.objs(from_block).graphics.pein)
    from_block=lnk.from(1);
    from_port=lnk.from(2);
  end
  from=[from_block from_port from(3)]
  from_port_id=ports_association(vectorfind(ports_association(:,[1:2,4:5]),[from(:,1:2) -ones(size(from,1)) o.ct(2)],'r'),3)
  if o.ct(2)==-1 then class="ControlFlow",else class="DataFlow",end
  
  if (class=="DataFlow"| scs_m.objs(from_block).gui=='IFTHEL_f') then
    if (scs_m.objs(o.to(1)).gui<>'SPLIT_f') then
      to_port_id=ports_association(vectorfind(ports_association(:,[1:2,4:5]),[to(:,1:2) ones(size(to,1)) o.ct(2)],'r'),3)
      xml=[xml;    
	 '<Signal '+newxmlid()+'>' 
         '     <srcPort type=""gaxml:pointer"">'+string(from_port_id)+'</srcPort>'
	 '     <dstPort type=""gaxml:pointer"">'+string(to_port_id)+'</dstPort>'
	 '</Signal>']
    end
  end
endfunction
