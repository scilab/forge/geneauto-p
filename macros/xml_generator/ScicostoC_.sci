// "Scicos to C" menu handler  

// Authors : Fady Nassif, Serge Steer. INRIA. 2007-2009. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function ScicostoC_()

   // k = [] ; //** index of the CodeGen source superbloc candidate
   // xc = %pt(1); //** last valid click position 
   // yc = %pt(2); 
   errcatch(-1)
   %pt = [];

     // Is there any selected super block?
    //----------------------------------
    
    if ~exists("blk") then
    	// No selected block
    	// Note: Currently only conversion from superblocks is supported, 
    	// but we leave the message more general here 
    	message("Please select a block!")
    	return
    end 
    
    if typeof(blk)<>'Block' then return,end // no selected block
    if and(blk.model.sim(1)<>['super','asuper']) then
       message("Code Generation only works for a Superblock!")
       return
    end
    
    // Select generated files location
    //--------------------------------
    msg='Select a destination directory for the generated files '+..
	 'Warning existing files in this directory may be overwritten';
    dest=uigetdir(title=msg)
    if length(dest)==0 then return,end //canceled by user
    dest=pathconvert(dest,%t,%f)
    [status,mess]=mkdir(dest)
    if status==0 then message(mess),return,end

    // Analyse the super block and generate codes (generic and Scicos specific)
    // ------------------------------------------------------------------------
    
    // FIXME: for Xcos we only need to store the block uuid ?
    selblocknumber = [];
    ga_set_context(scs_m,selblocknumber) //for error reporting

    [ok,guiname,freof]=ga_codegen(ga_replaceports(blk.model.rpar),dest)

    // FIXME: for Xcos we only need to store the block uuid ?
    ga_clear_context()
    
    if ~ok then return,end
endfunction

