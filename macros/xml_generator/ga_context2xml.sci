function [xml,matparm]=ga_context2xml(scs_m,name,Bn,matparm)
// Generate XML GeneAuto representation of a Scicos superblock context
// Arguments:
// scs_m   : scicos data structure of a superblock
// name    : superblock name
// Bn      : string, the base for block naming
// matparm :    
//
// xml   : column vector of strings (the GeneAuto XML representation of the superblock)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  xml=[];
  if scs_m.props.context==[] then return;end
// Handling System Context
  //------------------------
  //// get names and values of the locally defined constants
  [%scicos_context,ierr]=script2var(scs_m.props.context,%scicos_context)
  names = getfield(1,%scicos_context); names=names(3:$);
  if names==[] then return,end
  //rename variables to avoid name conflit with hierachical contexts
  //matparm(:,1) contains the original names and matparm(:,2) contains
  //the modified names; matparm(:,3) contains the absolute id 
  if %f
  for i=1:size(names,'*')
    new=find(matparm(:,1)==string(names(i)));
    if new<>[] then 
      //the variable was defined in a context above and is redefined for
      //current level and the the sublevels, so it is possible to remove
      //the previous name correspondance
      matparm(new,:)=[]; 
    end
  end
  end
  for i=size(names,'*'):-1:1
    value=getfield(i+2,%scicos_context);
    matparm=[matparm;[names(i), Bn+'_'+names(i),string(getxmlid())]];
    xml=[xml;
	 contextvalue2xml(Bn+'_'+names(i),value)];
  end

endfunction
