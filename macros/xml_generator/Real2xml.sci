function [txt,ok]=Real2xml(value)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  id1=newxmlid()
  t=formatrealnumber(value)
  litValue=' litValue=""'+t+'""'
  ke=strindex(t,'D')
  if ke==[] then
    scientificValue=' scientificValue=""false""'
    exponent=' exponent=""0""'
    ke=length(t)+1
  else
    scientificValue=' scientificValue=""true""'
    exponent=' exponent=""'+part(t,ke+1:length(t))+'""'
  end
  kdot=strindex(t,'.')
  if kdot<>[] then
     fractionalPart=' fractionalPart=""'+part(t,kdot+1:ke-1)+'""'
     integerPart=' integerPart=""'+part(t,1:kdot-1)+'""'
  else
     fractionalPart=' fractionalPart=""0""'
     integerPart=' integerPart=""'+part(t,1:ke-1)+'""'
  end
  [xmls,ok]=scalardatatype2xml(value)
  if ~ok then txt=[];setxmlid(idstart),return,end
  txt=['<RealExpression  '+exponent+fractionalPart+' '+id1+integerPart+litValue+scientificValue+' >'
       '    '+xmls
       '</RealExpression>']

endfunction
