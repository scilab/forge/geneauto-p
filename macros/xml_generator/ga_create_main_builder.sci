function  ok=ga_create_main_builder(main_path,src_path)
//Create the builder.sce file which will be used by scilab to build the
//dynamic library associated with the C code (Scicos simulation
//function, and generated code)

// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  txt=['mode(-1)';
       'P=get_absolute_file_path(''builder.sce'');'
       'exec(P+''src/builder.sce'');']
  ok=execstr('mputl(txt,main_path+''builder.sce'')','errcatch')==0
  if ~ok then message(lasterror()),end
endfunction

