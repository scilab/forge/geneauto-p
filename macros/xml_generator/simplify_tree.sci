
function tree=simplify_tree(tree)
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  select typeof(tree)
  case "operation"
    if tree.operator=="rc" then
      rowelements=list(tree.operands(2));
      left=tree.operands(1);
      while typeof(left)=="operation"&left.operator=="rc" then
	rowelements(0)=left.operands(2);
	left=left.operands(1);
      end
      rowelements(0)=left;
      tree.operands=rowelements
    elseif tree.operator=="cc" then
      rowelements=list(tree.operands(2))
      left=tree.operands(1);
      while typeof(left)=="operation"&left.operator=="cc" then
	rowelements(0)=left.operands(2);
	left=left.operands(1);
      end
      rowelements(0)=left;
      tree.operands=rowelements
    else
      for k=1:size(tree.operands)
	tree.operands(k)=simplify_tree(tree.operands(k))
      end
    end
  case "funcall" then
    for k=1:size(tree.rhs)
      tree.rhs(k)=simplify_tree(tree.rhs(k))
    end
  case "variable" then

  case "cste" then
  
  else
    disp(typeof(tree))
  end
 
endfunction
