function [routines,IntFunc]=ga_create_palette(Path)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29
// June 2007
  create_palette=create_palette;
  load SCI/modules/scicos/macros/lib;
  scicos_ver=get_scicos_version();
  list_blocks_from_scicos=[SCI+'/modules/scicos_blocks/macros/Sources/SampleCLK.sci';
			   SCI+'/modules/scicos_blocks/macros/Sources/CONST_m.sci';
			   SCI+'/modules/scicos_blocks/macros/Sources/IN_f.sci';
			   SCI+'/modules/scicos_blocks/macros/Sources/CLKINV_f.sci';
			   SCI+'/modules/scicos_blocks/macros/Sinks/OUT_f.sci';
			   SCI+'/modules/scicos_blocks/macros/Linear/SUMMATION.sci';
			   SCI+'/modules/scicos_blocks/macros/Linear/GAINBLK.sci';
			   SCI+'/modules/scicos_blocks/macros/Linear/DOLLAR_m.sci';
			   SCI+'/modules/scicos_blocks/macros/NonLinear/SIGNUM.sci';
			   SCI+'/modules/scicos_blocks/macros/NonLinear/PRODUCT.sci';
			   SCI+'/modules/scicos_blocks/macros/NonLinear/ABS_VALUE.sci';
			   SCI+'/modules/scicos_blocks/macros/NonLinear/TrigFun.sci';
			   SCI+'/modules/scicos_blocks/macros/NonLinear/MAXMIN.sci';
			   SCI+'/modules/scicos_blocks/macros/MatrixOp/MATMUL.sci';
			   SCI+'/modules/scicos_blocks/macros/IntegerOp/INTMUL.sci';
			   SCI+'/modules/scicos_blocks/macros/IntegerOp/CONVERT.sci';
			   SCI+'/modules/scicos_blocks/macros/Events/IFTHEL_f.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/DEMUX.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/GOTO.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/FROM.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/GotoTagVisibility.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/M_SWITCH.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/MUX.sci';
			   SCI+'/modules/scicos_blocks/macros/Branching/SWITCH2_m.sci';
			   SCI+'/modules/scicos_blocks/macros/Misc/RELATIONALOP.sci';
			   SCI+'/modules/scicos_blocks/macros/Misc/TEXT_f.sci'
			  ]
  Path=pathconvert(Path,%t,%t)
  PalName=basename(part(Path,1:length(Path)-1))
  to_del=[]
  lisf=listfiles(Path+'*.sci')
  lisf=[lisf;list_blocks_from_scicos]
  for i=1:size(lisf,'*')
    fil=lisf(i)
    ierror=execstr('getf(fil)','errcatch')
    if ierror <>0 then
      to_del=[to_del i];
    end
  end
  lisf(to_del)=[];
  routines=build_palette(lisf,Path,PalName);IntFunc=lisf
endfunction
