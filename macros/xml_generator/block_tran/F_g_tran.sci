function [name,label,values,EXPRS]=F_g_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name='F';
  label=['EXTH1';'PARH1'];
  values=list('on',o.model.opar);
  EXPRS=['',o.graphics.exprs];
endfunction
