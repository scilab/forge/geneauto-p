function [name,label,values,EXPRS]=SOM_f_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29
// June 2007
  name='Sum'
  label=['Inputs' 'OutDataTypeMode']
  values=list('+++','Inherit via internal rule')
  EXPRS=['' '']
endfunction
