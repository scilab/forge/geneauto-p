function [name,label,values,EXPRS]=CONST_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'Constant'
  values=list(o.model.rpar,'Inherit from ''Constant value''','off')
  label=['Value';'OutDataTypeMode';'VectorParams1D']
  
  EXPRS=o.graphics.exprs
  for k=2:size(values),EXPRS=[EXPRS sci2exp(values(k),0)],end
endfunction
