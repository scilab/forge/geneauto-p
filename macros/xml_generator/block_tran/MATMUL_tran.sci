function [name,label,values,EXPRS]=MATMUL_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
name='Product'
rule=['Matrix(*)','Element-wise(.*)']
if o.model.ipar == [] then
    rule_n=1
else
    //saclar matrix product seen as element wise product
    rule_n=min(o.model.ipar,2)
end

if rule_n==1 then//matrix product
  if  or(o.model.sim(1)=='matmul_m') then
    OutDataTypeMode="double"
  elseif  part(o.model.sim(1),1:10)=='matmul_i32' then
    OutDataTypeMode="int32"
  elseif  part(o.model.sim(1),1:10)=='matmul_i16' then
    OutDataTypeMode="int16"
  elseif  part(o.model.sim(1),1:9)=='matmul_i8' then
    OutDataTypeMode="int8"
  elseif  part(o.model.sim(1),1:11)=='matmul_ui32' then
    OutDataTypeMode="uint32"
  elseif  part(o.model.sim(1),1:11)=='matmul_ui16' then
    OutDataTypeMode="uint16"
  elseif  part(o.model.sim(1),1:10)=='matmul_ui8' then
    OutDataTypeMode="uint8"
  end 
else
  OutDataTypeMode="Same as first input"
end
label=['Multiplication','Inputs','OutDataTypeMode',"OutDataType"]
values=list(rule(rule_n),'2',OutDataTypeMode, "sfix(16)");
EXPRS=['' '' '',''];
endfunction
