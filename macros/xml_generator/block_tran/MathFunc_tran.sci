function [name,label,values,EXPRS]=MathFunc_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  OPER=['exp';'log';'10^u';'log10';'magnitude^2';..
                    'square';'sqrt';'pow';'conj';'reciprocal';..
                    'hypot';'rem';'mod';'transpose';'hermitian']
  num=o.model.ipar
  name='Math';
  label=['Function' 'OutputSignalType']
  values=list(OPER(num),'auto');
  EXPRS=['' '']
endfunction
