function [name,label,values,EXPRS]=GAIN_f_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
values=list();
name='Gain'
label=['Gain';'Multiplication']
values(1)=o.model.rpar
if size(values(1),'*')==1 then
  values(2)='Element-wise(K.*u)'
else 
  values(2)='Matrix(K*u)'
end
EXPRS(2)=''
EXPRS(1)=o.graphics.exprs(1)
endfunction
