function [name,label,values,EXPRS]=BascR_tran(o)
  name='BASCR';
  if o.model.rpar<>1 then 
    label=[];values=list();EXPRS=[]
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  else
     label=['EXTH1';'EXTH2';'PARH1';'PARH2'];
     values=list('on','on',o.model.ipar(1),o.model.ipar(2));
     bool=['false','true']
     o.graphics.exprs(1)=bool(eval(o.graphics.exprs(1))+1);
     o.graphics.exprs(2)=bool(eval(o.graphics.exprs(2))+1);
     EXPRS=['';'';'';'']
  end
endfunction
