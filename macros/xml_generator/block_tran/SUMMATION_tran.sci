function [name,label,values,EXPRS]=SUMMATION_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
name='Sum'
inputs='';
if size(o.model.ipar,'*')==1 then
  for i=1:o.model.ipar
     inputs=inputs+'+'
  end
else
  for i=1:size(o.model.ipar,'*')
    if o.model.ipar(i)==1 then
      inputs=inputs+'+'
    elseif o.model.ipar(i)==-1 then
      inputs=inputs+'-'
    end
  end
end
label=['Inputs' 'OutDataTypeMode']
values=list(inputs,'Inherit via internal rule')
EXPRS=['' '']

endfunction
