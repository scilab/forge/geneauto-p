function [name,label,values,EXPRS]=GOTO_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'Goto'
  label= ['GotoTag';'TagVisibility'];
  op=['local','scoped','global']
  values=list(o.model.opar(1),op(o.model.ipar))
  EXPRS=o.graphics.exprs
endfunction
