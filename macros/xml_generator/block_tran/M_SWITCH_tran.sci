function [name,label,values,EXPRS]=M_SWITCH_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'MultiPortSwitch'
  label= 'Input';
  values=list(size(o.model.in,'*'))
  EXPRS=o.graphics.exprs(1)
endfunction
