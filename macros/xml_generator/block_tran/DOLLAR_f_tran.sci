function [name,label,values,EXPRS]=DOLLAR_f_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'UnitDelay'
  label='InitialValue'
  values= list(o.model.dstate)
  EXPRS=o.graphics.exprs(1)
endfunction
