function [name,label,values,EXPRS]=FROM_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'From'
  label= 'GotoTag';
  values=list(o.model.opar(1))
  EXPRS=o.graphics.exprs
endfunction
