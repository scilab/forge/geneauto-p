function [name,label,values,EXPRS]=ACTION_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
action=['then';'else']
name='ActionPort'
//label=['ActionType';'InitializeStates']
//values=list(action(o.model.ipar),'held')
//EXPRS=[sci2exp(values(1)),''];
label='InitializeStates'
values=list('held')
EXPRS=''
endfunction
