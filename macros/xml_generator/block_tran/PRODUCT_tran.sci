function [name,label,values,EXPRS]=PRODUCT_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007  
name='Product'
label=['Multiplication','Inputs',"OutDataTypeMode"]
val='';
for i=1:size(o.model.ipar,'*')
  if o.model.ipar(i)==1 then
    val=val+'*'
  elseif o.model.ipar(i)==-1 then
    val=val+'/'
  end
end
values=list('Element-wise(.*)',val,"Inherit via internal rule")
EXPRS=['','',''];

endfunction
