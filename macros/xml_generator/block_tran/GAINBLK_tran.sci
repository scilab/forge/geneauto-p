function [name,label,values,EXPRS]=GAINBLK_tran(o)
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  values=list();
  name='Gain'
  label=['Gain';'Multiplication']
  if o.model.rpar==[] then
    values(1)=o.model.opar(1)
  else
    values(1)=o.model.rpar
  end
  if size(values(1),'*')==1 then
    values(2)='Element-wise(K.*u)'
  else 
    values(2)='Matrix(K*u)'
  end
  EXPRS=[o.graphics.exprs(1) '']
endfunction
