// Call Gene-Auto to convert the model in Gene-Auto System Model XML to C
  
// Authors : Fady Nassif, Serge Steer. INRIA. 29 June 2007. GPL Version 3
// Contributors : Daniel Tuulik. Andres Toom. IB Krates. 08 April 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function ok=ga_xml2c(xml_path,name,src_path)

  tmp='geneauto.err';
  xml_file='""'+xml_path+name+'.gsm.xml'+'""';
  m_file='-m ""'+xml_path+name+'.m'+'""';
  launcher_path=ga_launcher_path()
  if launcher_path==[] then ok=%f,return,end
  
  // Compute the output path
  // We set the output path explicitly. Otherwise, Gene-Auto removes only one extension
  output_path=xml_path;
  if MSDOS then separ="\"; else separ="/"; end
  // Avoid adding false presecing "/" (matters in Unix-Linux) 
  if ~isempty(output_path) then output_path = output_path + separ; end
  output_path  = output_path + name + "_ga"
     
  output_param = "-O """+output_path+""""

  // Convert the model encoded in .xml to C using Gene-Auto	  
  // Form the required shell instruction
  instr='java  -classpath '+launcher_path+' geneauto.launcher.GALauncherSCICOSOpt '+xml_file+' '+output_param+' 2>'+tmp  
  // Run Gene-Auto
  printf("Running Gene-Auto:\n")
  printf(" > " + instr + "\n\n")  
  stat=unix(instr)
  
  t=mgetl(tmp)
  //mdelete(tmp)
  if stat<>0 then
     k=grep(t,"[Gene-Auto] StateFlow generator sftr.exe")
     if k<>[] then t(k)==[],end
    [k,w]=grep(t,['[GENEAUTO - ', 'ERROR]'])
    if k<>[] then
      t=t(k(1));w=w(1)
      select w
      case 1 then
	i1=strindex(t,'block (')+8
	i2=strindex(t,')')-1;
      case 2 then
	i1=strindex(t,'Block B')+7
	i2=strindex(t,'(')-1;
      end
      hilite_path([selblocknumber evstr(tokens(part(t,i1:i2),'_'))],%f)
      message(t)
    else
      message(t)
    end	
    ok=%f
  else
    //the generated C codes are created in a subdirectory under xml_path named name+'.gsm'
    //move them in src_path // Disable move to src DT 06.04.2011
    result = messagebox('See Gene-Auto log file ?', "modal", "question", ["Yes" "No"])
    if result==1 then 
      if MSDOS then scinotes(xml_path+name+'_ga/tmp/geneauto.log.txt');
      else scinotes(xml_path+'tmp/geneauto.log.txt');
      end
    end
    // Disable move to src DT 06.04.2011
    // if ~MSDOS 
      // unix_s('mv '+xml_path+'*.c '+src_path)
      // unix_s('mv '+xml_path+'*.h '+src_path)
    // else
      // F=listfiles([xml_path+name+'.xml_ga\*.c',xml_path+name+'_ga\*.h'])
      // for f=F
         // instr='move /Y '+'""'+f+'""  '+'""'+src_path+'""'
	 // instr2='del '+'""'+f+'""'
         // //disp(instr)
         // unix_s(instr)
	 // unix_s(instr2)
      // end
    //end    
    // Disable move to src DT 06.04.2011
    ok=%t
  end
endfunction
