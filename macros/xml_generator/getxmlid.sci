
function id=getxmlid()
  // Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  global id_num
  id=id_num
endfunction
