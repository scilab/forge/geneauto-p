
function [txt,ok]=dimensions2xml(dims)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  txt=['<dimensions type=""gaxml:collection"">']
  for k=1:size(dims,'*')
    [xmlv,ok]=Integer2xml(uint8(dims(k)))
    if ~ok then txt=[];setxmlid(idstart),return,end
     txt=[txt;
	'    '+xmlv];
  end
  txt=[txt;'</dimensions>']
endfunction
