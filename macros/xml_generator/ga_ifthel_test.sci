function [cur_scs_m,ok]=ga_ifthel_test(cur_scs_m)
// Checks if "if then else" blocks outputs are connected to a superblock
// and only one.
// Add an action block into the connected superblock.
  
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
  global  lk  ksel 
  
  ok=%t
  Action=["then","else"]
  for k=1:size(cur_scs_m.objs)
    o=cur_scs_m.objs(k)
    if typeof(o)=='Block' then
      if o.gui=='IFTHEL_f' then
	ln=o.graphics.peout'
	i=0;
	for k=ln
	  i=i+1;
	  if k<>0 then
	    curlnk=cur_scs_m.objs(k);
	    postblk=cur_scs_m.objs(curlnk.to(1))
	    if postblk.gui<>"SUPER_f" then
	      msg = ["An if block can only be connected to an action subsystem";..
		       "and may not be connected to more than one action subsystem"]
	      hilite_path([lk k], msg, %f);
          message(msg);
	      ok=%f
	      return; 
	    end
	    //Add an ACTION block in the connected superblock
	    cur_scs_m_1=postblk.model.rpar
	    if typeof(cur_scs_m_1.objs($))=='Block' then
	      if (cur_scs_m_1.objs($).gui<>'Action') then
	        cur_scs_m_1.objs($+1)=ACTION("define");
	        cur_scs_m_1.objs($).model.ipar=i;
	      end
	    else
	      cur_scs_m_1.objs($+1)=ACTION("define");
	      cur_scs_m_1.objs($).model.ipar=i;
	    end	    
	    //[cur_scs_m_1,ok]=ga_ifthel_test(cur_scs_m_1)
	    postblk.model.rpar=cur_scs_m_1
	    cur_scs_m.objs(curlnk.to(1))=postblk;
	  end
	end 
      elseif o.gui=="SUPER_f" then
	lk=[lk k]
	[s1,ok]=ga_ifthel_test(o.model.rpar)
	lk=lk(1:$-1)
	if ~ok then return,end
	cur_scs_m.objs(k).model.rpar=s1
      end
    end
  end
endfunction


