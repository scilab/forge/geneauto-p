function [txt,ok]=contextvalue2xml(name,value)
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  if size(value,'*')==1 then
    id1=newxmlid()
    id2=newxmlid()
    [xmld,ok]=Scalar2xml(value)
    if ~ok then txt=[];setxmlid(idstart),return,end
    [xmltyp,ok]=arraydatatype2xml(value)
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=['<Variable_SM '+id1+' isConst=""true"" isOptimizable=""false"" isStatic=""false"" isVolatile=""true"" name=""'+name+'"">'
	 '    <initialValue type=""gaxml:object"">'
	 '        <ExpressionValue '+id2+'>'
         '            <value type=""gaxml:object"">'
	 '                '+xmld
	 '            </value>'
	 '        </ExpressionValue>'
	 '    </initialValue>'
	 '    <scope type=""gaxml:object"">'
	 '        <VariableScope name=""ExportedVariable"" ordinal=""6"" type=""gaxml:enum""/>'
	 '    </scope>'
	 '    '+xmltyp
	 '</Variable_SM>']
  else
    id1=newxmlid()
    [xmld,ok]=value2xml(value)
    if ~ok then txt=[];setxmlid(idstart),return,end
    [xmltyp,ok]=arraydatatype2xml(value)
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=['<Variable_SM '+id1+' isConst=""true"" isOptimizable=""false"" isStatic=""false"" isVolatile=""true"" name=""'+name+'"">'
	 '    <initialValue type=""gaxml:object"">'
	 '        '+xmld
	 '    </initialValue>'
	 '    <scope type=""gaxml:object"">'
	 '        <VariableScope name=""ExportedVariable"" ordinal=""6"" type=""gaxml:enum""/>'
	 '    </scope>'
	 '    '+xmltyp
	 '</Variable_SM>']
  end
endfunction
