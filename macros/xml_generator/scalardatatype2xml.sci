
function [txt,ok]=scalardatatype2xml(value)  
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  ok=%t
  select type(value)
  case 1 then
    txt=['<dataType type=""gaxml:object"">'
	 '    <TRealDouble '+newxmlid()+' />'
	 '</dataType>']
  case 8 then
    ityp=inttype(value)
    nbits=modulo(ityp,10)*8
    if ityp>10 then
      txt=['<dataType type=""gaxml:object"">'
	   '    <TRealInteger '+newxmlid()+' nBits=""'+string(nbits)+'""/>'
	   '</dataType>']
    else
      txt=['<dataType type=""gaxml:object"">'
	   '    <TRealInteger '+newxmlid()+' nBits=""'+string(nbits)+'"" signed=""true""/>'
	   '</dataType>']
    end
  else 
    txt=[]; ok=%f
    errormessage('Variable with Scilab type '+string(typ)+' are not yet handled')
    return
  end

endfunction
