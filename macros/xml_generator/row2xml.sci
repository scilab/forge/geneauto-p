
function [txt,ok]=row2xml(value)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  txt=['<ListExpression '+newxmlid()+'>'
       '    <expressions type=""gaxml:collection"">']
  for k=1:size(value,2)
    [xmls,ok]=Scalar2xml(value(k))
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=[txt;
	 '        '+xmls]
  end
  [xmltyp,ok]=arraydatatype2xml(value,size(value,2))
  if ~ok then txt=[];setxmlid(idstart),return,end
  txt=[txt
       '    </expressions>'
       '    '+xmltyp
       '</ListExpression>']
endfunction
