function [ok,scs_m]=ga_change_atomic(scs_m)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
//change atomic blocks to non atomic 
ok=%t;
for i=1:size(scs_m.objs)
  o=scs_m.objs(i)
  x=typeof(o);
  if x=='Block' then
    if o.model.sim(1)=='asuper' then
       o.model.sim='super'
       [ok,scs_m1]=ga_change_atomic(o.model.rpar)
       o.model.rpar=scs_m1;
       scs_m.objs(i)=o;
    end
  end
end
endfunction

     
