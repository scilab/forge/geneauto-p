function t=formatrealnumber(v)
// Author :  Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  f=format()
  format(15)
  t=string(v)
  format(f(2))
endfunction
