function ninnout=under_connection(path_out,prt_out,nout,path_in,prt_in,nin,flagg)
// alert for badly connected blocks
// path_out : Path of the "from block" in scs_m
// path_in  : Path of the "to block" in scs_m
//!
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007  

  if path_in==-1 then
    msg = ['One of this block''s outputs has negative size';
      'Please check.']
    hilite_path(path_out, msg, %f);
    message(msg);
    ninnout=0
    return
  end

  if path_in==-2 then
    if flagg==1 then
      msg = ['The input port '+string(prt_out)+' of this block have a negative size.';
	'Please check.']
    elseif flagg==2 then 
      msg = ['The input port '+string(prt_out)+' of this block have a negative type.';
	'Please check.']
    else
      msg = ['The input port '+string(prt_out)+' of this block is invalid.';
	'Please check.']
    end
    hilite_path(path_out, msg, %f);
    message(msg);
    ninnout=0
    return
  end

  disp(path_in);disp(path_out)
  hilite_path(path_in, "Error", %f); hilite_path(path_out, "Error", %f)
   if flagg==1 then
    ninnout=evstr(dialog(['Hilited block(s) have connected ports ';
	'with  sizes that cannot be determined by the context';
	'what is the size of this link'],'[1,1]'))
  elseif flagg==2 then
    ninnout=evstr(dialog(['Hilited block(s) have connected ports ';
	'with  types that cannot be determined by the context';
	'what is the type of this link'],'1'))
  end
endfunction

