function  ok=ga_create_main_loader(main_path,src_path,sci_path,name)
//Create the loader.sce file which will be used by scilab to load all the
//stuff related to the Scicos block
  
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  
  txt=['mode(-1)';
       'P=get_absolute_file_path(''loader.sce'');'
       'exec(P+''src/loader.sce'');'
       'exec(P+''macros/'+name+'.sci'');']
  ok=execstr('mputl(txt,main_path+''loader.sce'')','errcatch')==0
  if ~ok then message(lasterror()),end
endfunction
