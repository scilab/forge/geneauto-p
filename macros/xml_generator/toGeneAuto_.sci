// "Scicos to GeneAuto" menu handler
  
// Authors : Fady Nassif, Serge Steer. INRIA. 29 June 2007. GPL Version 3
// Contributors : Daniel Tuulik, Andres Toom. IB Krates. 08 April 2011

// Copyright (C) INRIA 2007-2008
// Copyright (C) IB Krates 2011

function toGeneAuto_()

    FindSBParams=ga_FindSBParams; //use FindSBParams in xmlgenlib instead of the
                           		  //standard one in scicoslib
                           
   // k = [] ; //** index of the CodeGen source superbloc candidate
   // xc = %pt(1); //** last valid click position 
   // yc = %pt(2); 
   errcatch(-1)
   %pt = [];
   
   // Is there any selected super block?
   //----------------------------------
    
    if ~exists("blk") then
    	// No selected block
    	// Note: Currently only conversion from superblocks is supported, 
    	// but we leave the message more general here 
    	message("Please select a block!")
    	return
    end 
    
    if typeof(blk)<>'Block' then return,end // no selected block
    if and(blk.model.sim(1)<>['super','asuper']) then
       message("Code Generation only works for a Superblock!")
       clear FindSBParams;return
    end
    
    // Select generated files location
    //--------------------------------
    msg='Select a destination directory for the generated files '+..
	 'Warning existing files in this directory may be overwritten';
    dest=uigetdir(title=msg)
    if length(dest)==0 then return,end //canceled by user
    dest=pathconvert(dest,%t,%f)
    [status,mess]=mkdir(dest)
    if status==0 then message(mess),return,end

    // Analyse the super block and generate codes (generic and Scicos specific)
    // ------------------------------------------------------------------------
    
    // FIXME: for Xcos we only need to store the block uuid ?
    selblocknumber = [];
    ga_set_context(scs_m,selblocknumber) //for error reporting

    main_scs_m=ga_replaceports(blk.model.rpar),
    xml_path=[];
    tit=strsubst(main_scs_m.props.title(1),' ','')
    if length(tit) == 0 then
      tit="Untitled";
    end
    
    //------------------------------------
    // - Main directory
    [status,mess]=mkdir(dest);
    if status==0 then message(mess),ok=%f,return,end
 
    // - generated XML file directory
    xml_path=pathconvert(dest,%t,%t)
    [status,mess]=mkdir(xml_path);
    if status==0 then message(mess),ok=%f,return,end
    
    // Analyse main_scs_m superbloc data structure and generate it's XML model
    //-----------------------------------------------------------------------
    [ok,xml,capt,actt,freof]=ga_scicos2xml(main_scs_m)
    if ~ok then return;end
 
    // - write the xml model
    filename=tit+'.gsm.xml';
    xml_file=xml_path+filename;
    mputl(xml,xml_file);

    mprintf('The XML file ' + filename + ' has been generated in the '"'+xml_path+''" directory.\n');

    
    // FIXME: for Xcos we only need to store the block uuid ?
    ga_clear_context()
    
    if ~ok then clear FindSBParams;return,end
endfunction

