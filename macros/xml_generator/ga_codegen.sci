// Code generation with Gene-Auto toolset
  
// Authors : Fady Nassif, Serge Steer. INRIA. 29 June 2007. GPL Version 3
// Contributors : Daniel Tuulik, Andres Toom. IB Krates. 08 April 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function [ok,guiname,freof]=ga_codegen(main_scs_m, dest)

  guiname='';freof=[];ok=%t;
  if argn(2)<2 then
    dest=pathconvert(TMPDIR,%t,%t)
  end
  
  //initialize lhs arguments in case of premature exit
  xml_path=[];src_path=[],sci_path=[]

  // Basename for the generated code
  //--------------------------------
  tit=strsubst(main_scs_m.props.title(1),' ','')
  if length(tit) == 0 then
    tit="Untitled";
  end

  // Create directory for generated files
  //------------------------------------
  // - Main directory
  [status,mess]=mkdir(dest);
  if status==0 then message(mess),ok=%f,return,end
 
  // - generated XML file directory
  xml_path=pathconvert(dest,%t,%t) //pathconvert(dest+'xml',%t,%t) DT 06.04.11 (new dir not nescessary)
  [status,mess]=mkdir(xml_path);
  if status==0 then message(mess),ok=%f,return,end
 
  // - Generated C code directory
  //src path is contraint by java code generator
//  src_path=pathconvert(dest+'xml/'+tit+'.xml_ga',%t,%t)
  src_path=pathconvert(dest,%t,%t) //pathconvert(dest+'src',%t,%t) DT 06.04.11 (new dir not nescessary)
  [status,mess]=mkdir(src_path);
  if status==0 then message(mess),ok=%f,return,end

  // - Generated Scilab code directory
  sci_path=pathconvert(dest,%t,%t) //pathconvert(dest+'macros',%t,%t) DT 06.04.11 (new dir not nescessary)
  [status,mess]=mkdir(sci_path);
  if status==0 then message(mess),ok=%f,return,end


  // Analyse main_scs_m superbloc data structure and generate it's XML model
  //-----------------------------------------------------------------------
  [ok,xml,capt,actt,freof]=ga_scicos2xml(main_scs_m)
  if ~ok then return;end

  // - write the xml model
  filename=tit+'.gsm.xml';
  xml_file=xml_path+filename;
  mputl(xml, xml_file);

  mprintf('The XML file ' + filename + ' has been generated in the '"'+xml_path+''" directory.\n');
  
  // Generate C code from XML model (generated code written in src_path)
  //--------------------------------------------------------------------
  ok=ga_xml2c(xml_path,tit,src_path)
  if ~ok then return;end
  mprintf('The block computational function '".c'"  file is generated in the '"'+src_path+''" directory\n');
  // Getting the name of atomic subsystem
  //-------------------------------------
  //AtomSub=ga_GetAtomSubName(main_scs_m);

  return; // Temporary disable for testing DT 25.03.2011
    
  // Generate C code of the Scicos simulation function of the C model
  //-----------------------------------------------------------------
  ok=ga_create_scicos_sim(src_path,tit,capt,actt);
  if ~ok then return;end
  mprintf('The block simulation function '".c'"  file is generated in the '"'+src_path+''" directory\n');
 
  // Generate the builder script for the  Scicos simulation function and the model code
  //-----------------------------------------------------------------------------------
  ok=ga_create_cbuilder(src_path,tit);
  if ~ok then return;end
  
  // Generate the interfacing function (Scilab code)
  //------------------------------------------------
  [ok,guiname]=ga_create_scicos_gui(tit,capt,actt,freof,sci_path)
  if ~ok then return;end
  mprintf('The block interface function '".sci'"  file is generated in the '"'+sci_path+''" directory\n');
  
  // Generate the main builder and loader scripts
  //---------------------------------------------
  ok=ga_create_main_builder(dest,src_path)
  if ~ok then return;end
  ok=ga_create_main_loader(dest,src_path,sci_path,guiname)
  if ~ok then return;end

  mprintf('The main builder and loader  scripts are generated in the '"'+dest+''" directory\n');

endfunction

