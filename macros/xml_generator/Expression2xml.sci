function [txt,ok]=Expression2xml(operands, operator)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  if operator=='ext' then
    if size(operands)==1&typeof(operands(1))=="variable" then
      [txt,ok]=VariableExpression2xml(operands(1).name)
    else
      errormessage('The extraction operation is not handled')
      txt=[]
      ok=%f
    end
  elseif operator=='rc' then
    [txt,ok]=rc2xml(operands)
  elseif operator=='cc' then
    txt=[],ok=%f,errormessage('The column concatenation operation is not handled')
    return
   [txt,ok]=cc2xml(operands)
  elseif size(operands)==2 then
    [txt,ok]=binaryops2xml(operands,operator)
  elseif size(operands)==1 then
    [txt,ok]=unaryops2xml(operands,operator)
  else
    txt=[];ok=%f
    errormessage('Operator ""'+operator+'"" is not yet supported")
  end
endfunction
