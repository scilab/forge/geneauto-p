// "Scicos to C and test" menu handler  

// Authors : Fady Nassif, Serge Steer. INRIA. 2007-2009. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function ScicostoCandtest_()
  
   Cmenu = []; // Clear the menu command
  
   // Temporarily disabled. Needs updating DT 06.04.11
   message ('Not available in the current version');
  
   return 

   // k = [] ; //** index of the CodeGen source superbloc candidate
   // xc = %pt(1); //** last valid click position 
   // yc = %pt(2); 
   errcatch(-1)
   %pt = []   ;
     // Is there any selected super block?
    //----------------------------------
    
    selblocknumber=Select(1); //Selected Scicos objectif any
    
    if selblocknumber==[] then return,end // no selected block
    if typeof(scs_m.objs(selblocknumber))<>'Block' then return,end // no selected block
    if and(scs_m.objs(selblocknumber).model.sim(1)<>['super','asuper']) then
       message("Code Generation only works for a Superblock ! ")
       return
    end
    
    // Select generated files location
    //--------------------------------
    msg='Select a destination directory for the generated files '+..
	 'Warning existing files in this directory may be overwritten';
    dest=uigetdir(title=msg)
    if length(dest)==0 then return,end //canceled by user
    dest=pathconvert(dest,%t,%f)
    [status,mess]=mkdir(dest)
    if status==0 then message(mess),return,end

    // Analyse the super block and generate codes (generic and Scicos specific)
    // ------------------------------------------------------------------------
    ga_set_context(scs_m,selblocknumber) //for error reporting
    [ok,guiname,freof]=ga_codegen(ga_replaceports(scs_m.objs(selblocknumber).model.rpar),dest)
    ga_clear_context()
    
    gh_curwin=scf(curwin) //**quick fix for sblock that contains scope
    if ~ok then clear FindSBParams;return,end
    
    // Put the generated Scicos block in the diagram for comparison
    // ------------------------------------------------------------
    
    // Next from here (until the end) commented out for testing purposes DT 31.03.2011
    
    // atomfreof=ga_add_sample_for_atomic(scs_m.objs(selblocknumber),scs_m);
    // if freof == [] then freof = atomfreof;
    // elseif ((atomfreof <> []) & (freof <> atomfreof)) then 
    //   message ('A sample clock is defined inside the atomic block');clear FindSBParams;return; 
    // end 
    // [ok,scs_m]=ga_plug_scicos_block(scs_m,selblocknumber,dest,guiname,freof)
    // if ~ok then clear FindSBParams;return,end
    // clear FindSBParams;
    // edited      = %t ;
    // needcompile = 4  ;
    // Cmenu = "Replot" ;
 
endfunction
