function [ok,xml,capt,actt,freof]=ga_scicos2xml(main_scs_m)
// Generate XML GeneAuto representation of a Scicos superblock 
// Arguments:
// main_scs_m : scicos data structure of a superblock.
//
// ok    : boolean true if no error occured
// xml   : column vector of strings (the GeneAuto XML representation of the superblock)
// capt  . input ports properties  
// actt  . output ports properties  
// freof : [sample time, offset] of the internal SampleCLK
  
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007 
  
  xml=[];capt=[],actt=[];freof=[];ok=%f; countobj=0 //initialize outputs

  if argn(2)<3 then flag=%f,end
  
  //Load scicos function library if necessary
  if exists('scicos_diagram')==0 then loadXcosLibs(), loadScicos(), end

  //Set global variables used for error reports
  //-------------------------------------------
  global  lk  ksel scs_msav bllst corinv k1
  lk=[];scs_m_sav=main_scs_m;


  //Check if main_scs_m is compliant with GeneAuto restrictions
  //-----------------------------------------------------------
  [ok,main_scs_m,freof]=ga_check_and_adapt_model(main_scs_m)
  if ~ok then ga_clear_globals();return,end
  [ok,main_scs_m1]=ga_change_atomic(main_scs_m)
  // Check connection and compute size and type of block ports
  //----------------------------------------------------------
  [bllst,connectmat,clkconnect,cor,corinv,ok]=c_pass1(main_scs_m1);
  if ~ok then ga_clear_globals();return,end
  
  // - Infer the size of input/output ports
  [ok,bllst]=adjust_inout_xml(bllst,connectmat); 
  if ~ok then ga_clear_globals();return,end
  
  // - Infer the input/output port types
  [ok,bllst]=adjust_typ_xml(bllst,connectmat);
  if ~ok then ga_clear_globals();return,end
  // - Set global variables  bllst corinv for use in ga_portblock2xml ga_regularblock2xml
//  global bllst corinv k1
  k1=0 //global block counter


  // XML generation
  //---------------
  //set an initialize the absolute xml objects counter
  setxmlid(1)
 
  // - Set superblock name and base for component naming
  tit=strsubst(main_scs_m.props.title(1),' ','')
  Bn='B'

  // - Set semi-global variables for block coordinates translation (see ga_block_pos)
  rect=round(GetBounds(main_scs_m)) //calcul du rectangle englobant
  H=(rect(4)-rect(2)) //hauteur du rectangle englobant
  
  // - System Context
  matparm=[]
  if~exists("%scicos_context") then
      %scicos_context = struct();
  end
  [xmlcontext,matparm]=ga_context2xml(main_scs_m,tit,Bn,matparm)
 
  // - Components of the superblock
  
  ports_association=[];
  id=newxmlid();
  [ok,xmlblocks,xmllinks,xmlcontextblocks,capt,actt]=ga_blocks2xml(main_scs_m,Bn,matparm)
  if ~ok then ga_clear_globals();return,end
  xmlcontext=[xmlcontext
	      xmlcontextblocks]
  capt=capt(find(capt(:,6)=='1'),:);
  actt=actt(find(actt(:,6)=='1'),:);
  // - Add the header and trailer
  V=string(datevec(datenum()));
  dt=strcat(V(1:3),'-')+' '+strcat(V(4:6),':');
  local_version= "Xcos - ";
  if or(fieldnames(scs_m) == "version") then
       local_version = local_version + scs_m.version;
  end
  if xmlcontext<>[] then
    xmlvars=['<variables type=""gaxml:collection"">'
	    '    '+xmlcontext
	    '</variables>']
  else
    xmlvars=[]
  end
  xml=['<GASystemModel xmlns=""http://www.geneauto.org/GASystemModel"" ' 
       '	       xmlns:gadt=""http://www.geneauto.org/GADataType"" '
       '	       xmlns:gaxml=""http://www.geneauto.org/GAXML"" '
       '	       lastSavedBy=""'+local_version+'"" '
       '	       lastSavedOn=""'+dt+'"" '
       '	       modelName=""'+tit+'"" modelType=""GASystemModel"" '
       '	       modelVersion=""6.3"" '
       '	       type=""gaxml:model""> '
       '  <TransformationHistory type=""gaxml:history""> '
       '    <Transformation readTime=""'+dt+'"" toolName=""Scicos"" writeTime=""'+dt+'""/> '
       '  </TransformationHistory> '
       '  '
       '  <SystemBlock directFeedThrough=""true"" '+id+' isVirtual=""false"" name=""'+tit+'"" type=""SubSystem""> '
       '    <blocks type=""gaxml:collection"">'
       '       '+xmlblocks
       '    </blocks>'
       '    <signals type=""gaxml:collection"">'
       '       '+xmllinks
       '    </signals>'
       '    <diagramInfo type=""gaxml:object"">'
       '      <DiagramInfo positionX=""0"" positionY=""0""/>'
       '    </diagramInfo>'
       '    '+xmlvars
       '  </SystemBlock>'
       '</GASystemModel>']

  // Set data for Scicos interface
  //------------------------------
  // - Input and output ports size and types
  [v,kkk]=gsort(capt(:,1),'g','i');
  capt=capt(kkk,:);
  [v,kkk]=gsort(actt(:,1),'g','i');
  actt=actt(kkk,:);
  ga_clear_globals();
endfunction

function ga_clear_globals()
  clearglobal bllst corinv k1
  clearglobal lk  ksel scs_msav 
  clearglobal id_num
endfunction

