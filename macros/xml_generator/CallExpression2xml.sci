
function  [txt,ok]=CallExpression2xml(rhs,name,lhsnb)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  if lhsnb<>1 then
     errormessage('Function call with multiple left hand side is not accepted')
     txt=[]
     ok=%f
     return
  end
  if size(rhs)<>1 then
      errormessage('Function call with multiple right hand side is not accepted')
     txt=[]
     ok=%f
     return
  end
  //convert input argument in xml

  if or(name==['int8','int16','int32','uint8','uint16','uint32']) then
    //CallExpression replaced by UnaryCastOperator+IntegerExpression
    [txt,ok]=Cast(name,rhs(1))
  else
    id=newxmlid()
    [xmlcall,ok]=ga_expressiontree2xml(rhs(1))
    if ~ok then txt=[];setxmlid(idstart),return,end

    [xmltyp,ok]=scalardatatype2xml(1.0) 
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=['<CallExpression '+id+' name=""'+name+'"">'
	 '    <arguments type=""gaxml:collection"">'
	 '        '+xmlcall
	 '    </arguments>'
	 '    '+xmltyp
	 '</CallExpression>']
  end
endfunction
function [txt,ok]=Cast(name,tree)
  //not yet implemented
  ok=%f
  txt=[]
endfunction
