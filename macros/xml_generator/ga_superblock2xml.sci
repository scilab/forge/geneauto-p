function [ok,xmldef,xmlcontext,ports_association]=ga_superblock2xml(scs_m,k,Bn,matparm,ports_association,atom)
// Generate XML GeneAuto representation of a Scicos sub-superblock 
// Arguments:
// scs_m : scicos data structure of a superblock
// k     : index of the sub-superblock inside the superblock 
// Bn      : string, the base for block naming
// matparm :    
//
// ok    : boolean true if no error occured
// xml   : column vector of strings (the GeneAuto XML representation of the superblock)
// mfile : column vector of strings (the Matlab instructions for parameter initialization)

// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007  
  xmldef=[];xmlcontext=[];
  global lk  ksel scs_msav 
  lk=[lk k];//>Set the current Superblock path (for error report)
  blk_num=k;
  o=scs_m.objs(k)
  atomvect=['off','on'];
  atomflg=atomvect(bool2s(atom)+1);
  // - Set semi-global variables for block coordinates translation (see ga_block_pos)
  rect=round(GetBounds(scs_m)) //calcul du rectangle englobant
  H=(rect(4)-rect(2)) //hauteur du rectangle englobant
 
 // - Set superblock name and base for component naming
  name=o.graphics.id;
  if length(name) == 0 then
      name=Bn+string(k);
  end
  Bn=name+'_',
  
  // Handling System Context
  [xmlcontext,matparm]=ga_context2xml(o.model.rpar,name,Bn,matparm)


  id_sup=newxmlid(); //the superblock id
  //Walk subtree in the Scicos objects hierarchy
  [ok,xmlblocks,llks,xmlcontexts,capt,actt]=ga_blocks2xml(o.model.rpar,Bn,matparm)
  if ~ok then lk(:,$)=[];return; end
  xmlcontext=[xmlcontext;xmlcontexts]
  
  o.model.in=-ones(size(capt,1),1);
  o.model.in2=-ones(size(capt,1),1);
  for i =1:size(capt,1)
     o.model.in(evstr(capt(i,1)))=evstr(capt(i,2));
     o.model.in2(evstr(capt(i,1)))=evstr(capt(i,3));
  end
  o.model.out=-ones(size(actt,1),1);
  o.model.out2=-ones(size(actt,1),1);
  for i =1:size(actt,1)
     o.model.out(evstr(actt(i,1)))=evstr(actt(i,2));
     o.model.out2(evstr(actt(i,1)))=evstr(actt(i,3));
  end
  
  if size(o.model.evtout,'*')>0 then
    message('A Subsystem cannot have an event output');ok=%f;lk(:,$)=[];return
  end
  [xmlin,pain,ok]=ga_InDataPorts2xml(o.model,blk_num)
  if ~ok then lk(:,$)=[]; return,end
  [xmlevt,paevt,ok]=ga_InControlPort2xml(o.model,blk_num)
  if ~ok then lk(:,$)=[];return,end
  [xmlout,paout,ok]=ga_OutDataPorts2xml(o.model,blk_num)
  if ~ok then lk(:,$)=[];return,end
  labs=['RTWSystemCode','AtomicSubSystem']
  vals=list('auto',atomflg);exprs=['','']

  xmldef=['<SystemBlock directFeedThrough=""true"" '+id_sup+' isVirtual=""false"" name=""'+name+'"" type=""SubSystem"">'
	  '     <blocks type=""gaxml:collection"">'
	  '          '+xmlblocks
	  '     </blocks>'
	  '     <signals type=""gaxml:collection"">'
	  '          '+llks
	  '     </signals>'
	  '     '+xmlin
	  '     '+xmlevt
	  '     '+xmlout
	  '     '+ga_parameters2xml(exprs,vals,labs)
	  '     '+ga_DiagramInfo(ga_block_pos(o))
	  '</SystemBlock>']

  ports_association=[ports_association;
		     pain
		     paevt
		     paout]
 
  
  //Reset the path
  lk(:,$)=[]

endfunction

