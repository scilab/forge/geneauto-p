function scs_m=ga_replaceports(scs_m,flg)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
  [lhs,rhs]=argn(0)
  if rhs<2 then flg=0;end    
  IN=[];OUT=[];clkIN=[];numa=[];numc=[];
  for i=1:size(scs_m.objs)
    if typeof(scs_m.objs(i))=='Block' then
      if scs_m.objs(i).gui=='CLKOUT_f' then
	if flg==0 then
	  ok=%f;%cpr=list()
	  message('Superblock should not have any activation output port.')
	  return
	else
	  scs_m.objs(i).gui='EVTOUT_f';
	  scs_m.objs(i).model.sim(1)='bidon'
	  clkIN=[clkIN scs_m.objs(i).model.ipar]
	end
      elseif scs_m.objs(i).gui=='IN_f' then
	//replace input ports by sensor blocks
	numc=numc+1
	scs_m.objs(i).gui='INPUTPORT';
	scs_m.objs(i).model.evtin=1
	scs_m.objs(i).model.sim(1)='Input';//+string(numc)
	IN=[IN scs_m.objs(i).model.ipar]
      elseif scs_m.objs(i).gui=='OUT_f' then
	//replace output ports by actuator blocks
	numa=numa+1
	scs_m.objs(i).gui='OUTPUTPORT';
	scs_m.objs(i).model.sim(1)='Output'//+string(numa)
	OUT=[OUT  scs_m.objs(i).model.ipar]
      elseif scs_m.objs(i).gui=='CLKINV_f' then
	//replace event input ports by  fictious block
	scs_m.objs(i).gui='EVTGEN_f';
	scs_m.objs(i).model.sim(1)='bidon'
	clkIN=[clkIN scs_m.objs(i).model.ipar];
	//elseif scs_m.objs(i).model.dep_ut(2)==%t then
	//check for time dependency PAS IICI
	//ok=%f;%cpr=list()
	//message('a block have time dependence.')
	//return
      end
    end
  end

  //Check if input/output ports are numered properly
  IN=-gsort(-IN);
  if or(IN<>[1:size(IN,'*')]) then 
    ok=%f;%cpr=list()
    message('Input ports are not numbered properly.')
    return
  end
  OUT=-gsort(-OUT);
  if or(OUT<>[1:size(OUT,'*')]) then 
    ok=%f;%cpr=list()
    message('Output ports are not numbered properly.')
    return
  end
  clkIN=-gsort(-clkIN);
  if or(clkIN<>[1:size(clkIN,'*')]) then 
    ok=%f;%cpr=list()
    message('Event input ports are not numbered properly.')
    return
  end

  //Check if there is more than one clock in the diagram
  szclkIN=size(clkIN,2);
  if szclkIN==0 then
    szclkIN=[]
  end
endfunction
