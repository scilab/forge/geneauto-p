
function txt=newxmlid()
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  global id_num
  txt='id=""'+string(id_num)+'""'
  id_num=id_num+1
endfunction
