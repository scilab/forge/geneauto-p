function [status,mess]=mkdir_rec(dest)
//Create a directory sequence. To be replaced by a standard Scilab function
// Author :  Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007   
 
  dest=pathconvert(dest,%t,%f)
  mess=''
  if MSDOS then sep='\',else sep='/',end
  tp=tokenpos(dest,sep)
  t=tokens(dest,sep)
  nt=size(tp,1);
  for k=nt:-1:1
    p=part(dest,1:tp(k,2)+1)
    if fileinfo(p)<>[] then 
      if isdir(p) then
	break
      else
	status=0
	mess='Directory '+p+' cannot  be created'
	return
      end
    end
  end
  if k==nt then
    status=2
    mess=''
    return
  end
  for i=k+1:nt
    ap=part(dest,1:tp(i-1,2)+1)
    nam=part(dest,tp(i,1):tp(i,2))
    [status,msg]=mkdir(ap,nam)
    if status==0 then break,end
  end
endfunction
