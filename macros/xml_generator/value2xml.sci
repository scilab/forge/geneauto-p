function [txt,ok]=value2xml(value)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29
// June 2007
  idstart=getxmlid()
  if size(value,'*')<=1 then//scalar or empty matrix case
    if or(type(value)==[1 8]) then
      // strange special case seems to be needed only for real scalars...
      id=newxmlid()
      if size(value,'*')==0 then 
	//this case appears in SinkBlocks.
	txt='<ExpressionValue '+id+'/>'
	ok=%t
      else
	[t,ok]=Scalar2xml(value)
	if ~ok then txt=[];setxmlid(idstart),return,end
	txt=['<ExpressionValue '+id+'>'
	     '    <value type=""gaxml:object"">'
	     '        '+t
	     '    </value>'
	     '</ExpressionValue>']
      end
    else
      [txt,ok]=Scalar2xml(value)
      if ~ok then txt=[];setxmlid(idstart),return,end
    end
  elseif size(value,1)==1 then //row vector case
    id1=newxmlid()
    id2=newxmlid()
    [xmlvect,ok]=vect2xml(value)
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=['<ExpressionValue '+id1+'>'
	 '    <value type=""gaxml:object"">'
	 '        <ListExpression '+id2+'>'
	 '            <expressions type=""gaxml:collection"">'
	 '                '+xmlvect
	 '            </expressions>'
	  '        </ListExpression>'
	 '    </value>'
	 '</ExpressionValue>']
  else //general case
    txt=['<ExpressionValue '+newxmlid()+'>'
	 '    <value type=""gaxml:object"">'
	 '        <ListExpression '+newxmlid()+'>'
	 '            <expressions type=""gaxml:collection"">']
    for k=1:size(value,1)
      [xmlrow,ok]=row2xml(value(k,:))
      if ~ok then txt=[];setxmlid(idstart),return,end
      txt=[txt
	   '                '+xmlrow]
    end
    [xmltyp,ok]=arraydatatype2xml(value)
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=[txt;
	 '            </expressions>'
	 '            '+xmltyp
	 '        </ListExpression>'
	 '    </value>'
	 '</ExpressionValue>']
  end
endfunction
